package com.vmo.notes.user.controller;

import com.vmo.notes.user.bo.UserBO;
import com.vmo.notes.user.form.UserForm;
import com.vmo.notes.user.service.impl.UserService;
import com.vmo.notes.userhistorypassword.bo.HistoryPasswordBO;
import com.vmo.notes.userhistorypassword.controller.HistoryPasswordController;
import com.vmo.notes.userhistorypassword.service.impl.HistoryPasswordService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private UserController userController;

    @Mock
    private HistoryPasswordService mockHistoryPasswordService;
    @Mock
    private UserService mockUserService;
    @Mock
    private BCryptPasswordEncoder mockBcryptEncoder;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(userController)
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();
    }

    @Test
    void testListUser() throws Exception {
        // Setup
        // Configure UserService.findAll(...).
        final List<UserBO> userBOS = Arrays.asList(new UserBO(0L, "username", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber"));
        when(mockUserService.findAll()).thenReturn(userBOS);

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(get("/api/users")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testListUser_UserServiceReturnsNoItems() throws Exception {
        // Setup
        when(mockUserService.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(get("/api/users")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).isEqualTo("[]");
    }

    @Test
    void testSaveUser() throws Exception {
        // Setup
        // Configure UserService.findByEmail(...).
        final UserBO userBO = new UserBO(0L, "username", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber");
        when(mockUserService.findByEmail("email")).thenReturn(userBO);

        when(mockBcryptEncoder.encode("password")).thenReturn("password");

        // Configure UserService.save(...).
        final UserBO userBO1 = new UserBO(0L, "username", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber");
        when(mockUserService.save(new UserForm(0L, "username", "password", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber", 0L, 0, 0)))
                .thenReturn(userBO1);

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(post("/api/signup")
                        .content("{}").contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED.value());
        verify(mockHistoryPasswordService).saveOrUpdate(any(HistoryPasswordBO.class));
    }

    @Test
    void testSaveUser_UserServiceFindByEmailReturnsNull() throws Exception {
        // Setup
        when(mockUserService.findByEmail("email")).thenReturn(null);
        when(mockBcryptEncoder.encode("password")).thenReturn("password");

        // Configure UserService.save(...).
        final UserBO userBO = new UserBO(0L, "username", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber");
        when(mockUserService.save(new UserForm(0L, "username", "password", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber", 0L, 0, 0)))
                .thenReturn(userBO);

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(post("/api/signup")
                        .content("{}").contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        verify(mockHistoryPasswordService).saveOrUpdate(any(HistoryPasswordBO.class));
    }
}
