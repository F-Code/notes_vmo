package com.vmo.notes.user.bean;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserBeanTest {

    private UserBean userBeanUnderTest;
    private UserBean userBeanTest;

    @BeforeEach
    void setUp() {
        userBeanUnderTest = new UserBean("account", "token");
        userBeanTest = new UserBean();
    }


    @Test
    void testGetAccount(){
        String expected = "account";
        String actual = userBeanUnderTest.getAccount();
        assertEquals(expected,actual);
    }
    @Test
    void testSetAccount(){
        String expected = "account";
        userBeanUnderTest.setAccount("account");
        String actual = userBeanUnderTest.getAccount();
        assertEquals(expected,actual);
    }
    @Test
    void testGetToken(){
        String expected = "account";
        String actual = userBeanUnderTest.getAccount();
        assertEquals(expected,actual);
    }
    @Test
    void testSetToken(){
        String expected = "token Test";
        userBeanUnderTest.setToken("token Test");
        String actual = userBeanUnderTest.getToken();
        assertEquals(expected,actual);
    }
}
