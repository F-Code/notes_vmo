package com.vmo.notes.user.bo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class UserBOTest {

    private UserBO userBOUnderTest;
    private UserBO userBO;

    @BeforeEach
    void setUp() {
        userBOUnderTest = new UserBO(0L, "username", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber");
        userBO = new UserBO();
    }

    @Test
    void testGetId() {
        Long expected = 0L;
        Long actual = userBOUnderTest.getId();
        assertEquals(expected, actual);
    }

    @Test
    void testSetId() {
        Long expected = 1L;
        userBOUnderTest.setId(1L);
        Long actual = userBOUnderTest.getId();
        assertEquals(expected, actual);
    }

    @Test
    void testGetUsername(){
        String expected = "username";
        String actual = userBOUnderTest.getUsername();
        assertEquals(expected,actual);
    }
    @Test
    void testSetUsername(){
        String expected = "username test";
        userBOUnderTest.setUsername("username test");
        String actual = userBOUnderTest.getUsername();
        assertEquals(expected,actual);
    }
    @Test
    void testGetFirstName(){
        String expected = "firstName";
        String actual = userBOUnderTest.getFirstName();
        assertEquals(expected,actual);
    }
    @Test
    void testSetFirstName(){
        String expected = "firstName Test";
        userBOUnderTest.setFirstName("firstName Test");
        String actual = userBOUnderTest.getFirstName();
        assertEquals(expected,actual);
    }
    @Test
    void testGetlastName(){
        String expected = "lastName";
        String actual = userBOUnderTest.getLastName();
        assertEquals(expected,actual);
    }
    @Test
    void testSetlastName(){
        String expected = "lastName test";
        userBOUnderTest.setLastName("lastName test");
        String actual = userBOUnderTest.getLastName();
        assertEquals(expected,actual);
    }
    @Test
    void testGetEmail(){
        String expected = "email";
        String actual = userBOUnderTest.getEmail();
        assertEquals(expected,actual);
    }
    @Test
    void testSetEmail(){
        String expected = "bichnt@gmail.com";
        userBOUnderTest.setEmail("bichnt@gmail.com");
        String actual = userBOUnderTest.getEmail();
        assertEquals(expected,actual);
    }

    @Test
    void testGetBirthDay(){
        Date expected =  new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime();
        Date actual = userBOUnderTest.getBirthDay();
        assertEquals(expected,actual);
    }
    @Test
    void testSetBirthDay(){
        Date expected = new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime();
        userBOUnderTest.setBirthDay(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        Date actual = userBOUnderTest.getBirthDay();
        assertEquals(expected,actual);
    }

    @Test
    void testGetPhoneNumber(){
        String expected = "phoneNumber";
        String actual = userBOUnderTest.getPhoneNumber();
        assertEquals(expected,actual);
    }
    @Test
    void testSetPhoneNumber(){
        String expected = "phoneNumber Test";
        userBOUnderTest.setPhoneNumber("phoneNumber Test");
        String actual = userBOUnderTest.getPhoneNumber();
        assertEquals(expected,actual);
    }
}
