package com.vmo.notes.user.service.impl;

import com.vmo.notes.user.bo.UserBO;
import com.vmo.notes.user.dao.UserDAO;
import com.vmo.notes.user.form.UserForm;
import com.vmo.notes.userhistorypassword.bo.HistoryPasswordBO;
import com.vmo.notes.userhistorypassword.dao.HistoryPasswordDao;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    private UserDAO mockUserDAO;
    @Mock
    private HistoryPasswordDao mockHistoryPasswordDao;
    @Mock
    private BCryptPasswordEncoder mockBcryptEncoder;

    @InjectMocks
    private UserService userServiceImplUnderTest;

    @Test
    void testLoadUserByUsername() {
        // Setup
        // Configure UserDAO.findByUsername(...).
        final UserBO userBO = new UserBO(0L, "username", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber");
        when(mockUserDAO.findByUsername("username")).thenReturn(userBO);

        // Configure HistoryPasswordDao.findPassword(...).
        final HistoryPasswordBO historyPasswordBO = new HistoryPasswordBO(0L, 0L, "password");
        when(mockHistoryPasswordDao.findPassword(0L)).thenReturn(historyPasswordBO);

        // Run the test
        final UserDetails result = userServiceImplUnderTest.loadUserByUsername("username");

        // Verify the results
    }

    @Test
    void testLoadUserByUsername_UserDAOReturnsNull() {
        // Setup
        when(mockUserDAO.findByUsername("username")).thenReturn(null);

        // Configure HistoryPasswordDao.findPassword(...).
        final HistoryPasswordBO historyPasswordBO = new HistoryPasswordBO(0L, 0L, "password");
        when(mockHistoryPasswordDao.findPassword(0L)).thenReturn(historyPasswordBO);

        // Run the test
    }

    @Test
    void testLoadUserByUsername_ThrowsUsernameNotFoundException() {
        // Setup
        // Configure UserDAO.findByUsername(...).
        final UserBO userBO = new UserBO(0L, "username", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber");
        when(mockUserDAO.findByUsername("username")).thenReturn(userBO);

        // Configure HistoryPasswordDao.findPassword(...).
        final HistoryPasswordBO historyPasswordBO = new HistoryPasswordBO(0L, 0L, "password");
        when(mockHistoryPasswordDao.findPassword(0L)).thenReturn(historyPasswordBO);

        // Run the test
    }

    @Test
    void testFindAll() {
        // Setup
        final List<UserBO> expectedResult = Arrays.asList(new UserBO(0L, "username", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber"));

        // Configure UserDAO.findAll(...).
        final Iterable<UserBO> userBOS = Arrays.asList(new UserBO(0L, "username", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber"));
        when(mockUserDAO.findAll()).thenReturn(userBOS);

        // Run the test
        final List<UserBO> result = userServiceImplUnderTest.findAll();

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testFindAll_UserDAOReturnsNoItems() {
        // Setup
        when(mockUserDAO.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final List<UserBO> result = userServiceImplUnderTest.findAll();

        // Verify the results
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testDelete() {
        // Setup
        // Run the test
        userServiceImplUnderTest.delete(0L);

        // Verify the results
        verify(mockUserDAO).deleteById(0L);
    }

    @Test
    void testFindOne() {
        // Setup
        final UserBO expectedResult = new UserBO(0L, "username", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber");

        // Configure UserDAO.findByUsername(...).
        final UserBO userBO = new UserBO(0L, "username", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber");
        when(mockUserDAO.findByUsername("username")).thenReturn(userBO);

        // Run the test
        final UserBO result = userServiceImplUnderTest.findOne("username");

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testFindByEmail() {
        // Setup
        final UserBO expectedResult = new UserBO(0L, "username", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber");

        // Configure UserDAO.findByEmail(...).
        final UserBO userBO = new UserBO(0L, "username", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber");
        when(mockUserDAO.findByEmail("email")).thenReturn(userBO);

        // Run the test
        final UserBO result = userServiceImplUnderTest.findByEmail("email");

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testFindById() {
        // Setup
        final UserBO expectedResult = new UserBO(0L, "username", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber");

        // Configure UserDAO.findById(...).
        final Optional<UserBO> userBO = Optional.of(new UserBO(0L, "username", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber"));
        when(mockUserDAO.findById(0L)).thenReturn(userBO);

        // Run the test
        final UserBO result = userServiceImplUnderTest.findById(0L);

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testFindById_UserDAOReturnsAbsent() {
        // Setup
        when(mockUserDAO.findById(0L)).thenReturn(Optional.empty());

        // Run the test
        assertThatThrownBy(() -> userServiceImplUnderTest.findById(0L)).isInstanceOf(NoSuchElementException.class);
    }

    @Test
    void testSave() {
        // Setup
        final UserForm user = new UserForm(0L, "username", "password", "firstName", "lastName", "email@gmail.com",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber", 0L, 0, 0);
        final UserBO expectedResult = new UserBO(0L, "username", "firstName", "lastName", "email@gmail.com",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber");
        when(mockBcryptEncoder.encode("password")).thenReturn("password");

        // Configure UserDAO.save(...).
        final UserBO userBO = new UserBO(0L, "username", "firstName", "lastName", "email@gmail.com",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber");
        when(mockUserDAO.save(new UserBO(0L, "username", "firstName", "lastName", "email@gmail.com",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber"))).thenReturn(userBO);

        // Run the test
        final UserBO result = userServiceImplUnderTest.save(user);

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
    }
}
