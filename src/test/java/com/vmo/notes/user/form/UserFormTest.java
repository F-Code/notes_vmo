package com.vmo.notes.user.form;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserFormTest {

    private UserForm userFormUnderTest;
    private UserForm userForm;

    @BeforeEach
    void setUp() {
        userFormUnderTest = new UserForm(0L, "username", "password", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber", 0L,0,0);
        userForm = new UserForm();
    }

    @Test
    void testGetId() {
        Long expected = 0L;
        Long actual = userFormUnderTest.getId();
        assertEquals(expected, actual);
    }

    @Test
    void testSetId() {
        Long expected = 1L;
        userFormUnderTest.setId(1L);
        Long actual = userFormUnderTest.getId();
        assertEquals(expected, actual);
    }

    @Test
    void testGetUsername() {
        String expected = "username";
        String actual = userFormUnderTest.getUsername();
        assertEquals(expected, actual);
    }

    @Test
    void testSetUsername() {
        String expected = "username Test";
        userFormUnderTest.setUsername("username Test");
        String actual = userFormUnderTest.getUsername();
        assertEquals(expected, actual);
    }

    @Test
    void testGetPassword() {
        String expected = "password";
        String actual = userFormUnderTest.getPassword();
        assertEquals(expected, actual);
    }

    @Test
    void testSetPassword() {
        String expected = "password Test";
        userFormUnderTest.setPassword("password Test");
        String actual = userFormUnderTest.getPassword();
        assertEquals(expected, actual);
    }

    @Test
    void testGetFirstName() {
        String expected = "firstName";
        String actual = userFormUnderTest.getFirstName();
        assertEquals(expected, actual);
    }

    @Test
    void testSetFirstName() {
        String expected = "firstName Test";
        userFormUnderTest.setFirstName("firstName Test");
        String actual = userFormUnderTest.getFirstName();
        assertEquals(expected, actual);
    }

    @Test
    void testGetLastName() {
        String expected = "lastName";
        String actual = userFormUnderTest.getLastName();
        assertEquals(expected, actual);
    }

    @Test
    void testSetLastName() {
        String expected = "lastName Test";
        userFormUnderTest.setLastName("lastName Test");
        String actual = userFormUnderTest.getLastName();
        assertEquals(expected, actual);
    }

    @Test
    void testGetEmail() {
        String expected = "email";
        String actual = userFormUnderTest.getEmail();
        assertEquals(expected, actual);
    }

    @Test
    void testSetEmail() {
        String expected = "email Test";
        userFormUnderTest.setEmail("email Test");
        String actual = userFormUnderTest.getEmail();
        assertEquals(expected, actual);
    }

    @Test
    void testGetBirthDay(){
        Date expected =  new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime();
        Date actual = userFormUnderTest.getBirthDay();
        assertEquals(expected,actual);
    }
    @Test
    void testSetBirthDay(){
        Date expected = new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime();
        userFormUnderTest.setBirthDay(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        Date actual = userFormUnderTest.getBirthDay();
        assertEquals(expected,actual);
    }

    @Test
    void testGetPhoneNumber() {
        String expected = "phoneNumber";
        String actual = userFormUnderTest.getPhoneNumber();
        assertEquals(expected, actual);
    }

    @Test
    void testSetPhoneNumber() {
        String expected = "phoneNumber Test";
        userFormUnderTest.setPhoneNumber("phoneNumber Test");
        String actual = userFormUnderTest.getPhoneNumber();
        assertEquals(expected, actual);
    }

    @Test
    void testGetDepartment(){
        Long expected = 0L;
        Long actual = userFormUnderTest.getDepartmentId();
        assertEquals(expected,actual);
    }
    @Test
    void testSetDepartment(){
        Long expected = 1L;
        userFormUnderTest.setDepartmentId(1L);
        Long actual = userFormUnderTest.getDepartmentId();
        assertEquals(expected,actual);
    }

    @Test
    void testGetPage() {
        Integer expected = 0;
        Integer actual = userFormUnderTest.getPage();
        assertEquals(expected, actual);
    }

    @Test
    void testSetPage() {
        Integer expected = 1;
        userFormUnderTest.setPage(1);
        Integer actual = userFormUnderTest.getPage();
        assertEquals(expected, actual);
    }

    @Test
    void testGetRecordPage() {
        Integer expected = 0;
        Integer actual = userFormUnderTest.getRecordPage();
        assertEquals(expected, actual);
    }

    @Test
    void testSetRecordPage() {
        Integer expected = 1;
        userFormUnderTest.setRecordPage(1);
        Integer actual = userFormUnderTest.getRecordPage();
        assertEquals(expected, actual);
    }
}
