package com.vmo.notes.userhistorypassword.bo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HistoryPasswordBOTest {

    private HistoryPasswordBO historyPasswordBOUnderTest;
    private HistoryPasswordBO historyPasswordBO;

    @BeforeEach
    void setUp() {
        historyPasswordBOUnderTest = new HistoryPasswordBO(0L, 0L, "password");
        historyPasswordBO = new HistoryPasswordBO();
    }
    @Test
    void testGetId(){
        Long expected = 0L;
        Long actual = historyPasswordBOUnderTest.getId();
        assertEquals(expected,actual);
    }
    @Test
    void testSetId(){
        Long expected = 1L;
        historyPasswordBOUnderTest.setId(1L);
        Long actual = historyPasswordBOUnderTest.getId();
        assertEquals(expected,actual);
    }
    @Test
    void testGetUserId(){
        Long expected = 0L;
        Long actual = historyPasswordBOUnderTest.getUserId();
        assertEquals(expected,actual);
    }
    @Test
    void testSetUserId(){
        Long expected = 1L;
        historyPasswordBOUnderTest.setUserId(1L);
        Long actual = historyPasswordBOUnderTest.getUserId();
        assertEquals(expected,actual);
    }
    @Test
    void testGetPassword(){
        String expected = "password";
        String actual = historyPasswordBOUnderTest.getPassword();
        assertEquals(expected,actual);
    }
    @Test
    void testSetPassword(){
        String expected = "1234567";
        historyPasswordBOUnderTest.setPassword("1234567");
        String actual = historyPasswordBOUnderTest.getPassword();
        assertEquals(expected,actual);
    }
}
