package com.vmo.notes.userhistorypassword.controller;

import com.vmo.notes.userhistorypassword.bean.HistoryPasswordBean;
import com.vmo.notes.userhistorypassword.bo.HistoryPasswordBO;
import com.vmo.notes.userhistorypassword.form.HistoryPasswordForm;
import com.vmo.notes.userhistorypassword.service.impl.HistoryPasswordService;
import com.vmo.notes.utils.DataTableResults;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class HistoryPasswordControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private HistoryPasswordController HistoryPasswordController;

    @Mock
    private HistoryPasswordService mockHistoryPasswordService;
    @Mock
    private BCryptPasswordEncoder mockBcryptEncoder;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(HistoryPasswordController)
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();
    }

    @Test
    void testFindById() throws Exception {
        // Setup
        // Configure HistoryPasswordService.findById(...).
        final HistoryPasswordBO historyPasswordBO = new HistoryPasswordBO(0L, 0L, "password");
        when(mockHistoryPasswordService.findById(0L)).thenReturn(historyPasswordBO);

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(get("/api/historyPassword/{id}", 0)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testProcessSearch() throws Exception {
        // Setup
        // Configure HistoryPasswordService.getDatatables(...).
        final DataTableResults<HistoryPasswordBean> historyPasswordBeanDataTableResults = new DataTableResults<>();
        historyPasswordBeanDataTableResults.setDraw("draw");
        historyPasswordBeanDataTableResults.setRecordsFiltered("recordsFiltered");
        historyPasswordBeanDataTableResults.setRecordsTotal("recordsTotal");
        historyPasswordBeanDataTableResults.setData(Arrays.asList());
        historyPasswordBeanDataTableResults.setFirst("first");
        when(mockHistoryPasswordService.getDatatables(any(HistoryPasswordForm.class)))
                .thenReturn(historyPasswordBeanDataTableResults);

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(post("/api/historyPassword/get-all")
                        .content("{}").contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testSaveHis() throws Exception {
        // Setup
        // Configure HistoryPasswordService.save(...).
        final HistoryPasswordBO historyPasswordBO = new HistoryPasswordBO(0L, 0L, "password");
        when(mockHistoryPasswordService.save(any(HistoryPasswordForm.class))).thenReturn(historyPasswordBO);

        when(mockBcryptEncoder.encode("password")).thenReturn("password");

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(post("/api/historyPassword")
                        .content("{}").contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testDelete() throws Exception {
        // Setup
        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(delete("/api/historyPassword/{id}", 0)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());

    }
}
