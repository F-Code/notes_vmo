package com.vmo.notes.userhistorypassword.bean;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HistoryPasswordBeanTest {

    private HistoryPasswordBean historyPasswordBeanUnderTest;
    private HistoryPasswordBean historyPasswordBean;

    @BeforeEach
    void setUp() {
        historyPasswordBeanUnderTest = new HistoryPasswordBean(0L, 0L, "password");
        historyPasswordBean = new HistoryPasswordBean();
    }
    @Test
    void testGetId(){
        Long expected = 0L;
        Long actual = historyPasswordBeanUnderTest.getId();
        assertEquals(expected,actual);
    }
    @Test
    void testSetId(){
        Long expected = 1L;
        historyPasswordBeanUnderTest.setId(1L);
        Long actual = historyPasswordBeanUnderTest.getId();
        assertEquals(expected,actual);
    }
    @Test
    void testGetUserId(){
        Long expected = 0L;
        Long actual = historyPasswordBeanUnderTest.getUserId();
        assertEquals(expected,actual);
    }
    @Test
    void testSetUserId(){
        Long expected = 1L;
        historyPasswordBeanUnderTest.setUserId(1L);
        Long actual = historyPasswordBeanUnderTest.getUserId();
        assertEquals(expected,actual);
    }
    @Test
    void testGetPassword(){
        String expected = "password";
        String actual = historyPasswordBeanUnderTest.getPassword();
        assertEquals(expected,actual);
    }
    @Test
    void testSetPassword(){
        String expected = "1234567";
        historyPasswordBeanUnderTest.setPassword("1234567");
        String actual = historyPasswordBeanUnderTest.getPassword();
        assertEquals(expected,actual);
    }
}
