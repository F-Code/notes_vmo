package com.vmo.notes.userhistorypassword.form;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HistoryPasswordFormTest {

    private HistoryPasswordForm historyPasswordFormUnderTest;
    private HistoryPasswordForm historyPasswordForm;

    @BeforeEach
    void setUp() {
        historyPasswordFormUnderTest = new HistoryPasswordForm(0L, 0L, "password", 0, 0);
        historyPasswordForm = new HistoryPasswordForm();
    }
    @Test
    void testGetId() {
        Long expected = 0L;
        Long actual = historyPasswordFormUnderTest.getId();
        assertEquals(expected, actual);
    }

    @Test
    void testSetId() {
        Long expected = 1L;
        historyPasswordFormUnderTest.setId(1L);
        Long actual = historyPasswordFormUnderTest.getId();
        assertEquals(expected, actual);
    }

    @Test
    void testGetUserId(){
        Long expected = 0L;
        Long actual = historyPasswordFormUnderTest.getUserId();
        assertEquals(expected,actual);
    }
    @Test
    void testSetUserId(){
        Long expected = 1L;
        historyPasswordFormUnderTest.setUserId(1L);
        Long actual = historyPasswordFormUnderTest.getUserId();
        assertEquals(expected,actual);
    }

    @Test
    void testGetPassword(){
        String expected = "password";
        String actual = historyPasswordFormUnderTest.getPassword();
        assertEquals(expected,actual);
    }
    @Test
    void testSetPassword(){
        String expected = "1234567";
        historyPasswordFormUnderTest.setPassword("1234567");
        String actual = historyPasswordFormUnderTest.getPassword();
        assertEquals(expected,actual);
    }


    @Test
    void testGetPage() {
        Integer expected = 0;
        Integer actual = historyPasswordFormUnderTest.getPage();
        assertEquals(expected, actual);
    }

    @Test
    void testSetPage() {
        Integer expected = 1;
        historyPasswordFormUnderTest.setPage(1);
        Integer actual = historyPasswordFormUnderTest.getPage();
        assertEquals(expected, actual);
    }

    @Test
    void testGetRecordPage() {
        Integer expected = 0;
        Integer actual = historyPasswordFormUnderTest.getRecordPage();
        assertEquals(expected, actual);
    }

    @Test
    void testSetRecordPage() {
        Integer expected = 1;
        historyPasswordFormUnderTest.setRecordPage(1);
        Integer actual = historyPasswordFormUnderTest.getRecordPage();
        assertEquals(expected, actual);
    }
}
