package com.vmo.notes.userhistorypassword.service.impl;

import com.vmo.notes.utils.CommonService;
import com.vmo.notes.utils.DataTableResults;
import com.vmo.notes.userhistorypassword.bean.HistoryPasswordBean;
import com.vmo.notes.userhistorypassword.bo.HistoryPasswordBO;
import com.vmo.notes.userhistorypassword.dao.HistoryPasswordDao;
import com.vmo.notes.userhistorypassword.form.HistoryPasswordForm;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class HistoryPasswordServiceTest {

    @Mock
    private HistoryPasswordDao mockHistoryPasswordDao;
    @Mock
    private CommonService mockCommonService;
    @Mock
    private BCryptPasswordEncoder mockBcryptEncoder;

    @InjectMocks
    private HistoryPasswordService historyPasswordServiceUnderTest;

    @Test
    void testFindAll() {
        // Setup
        // Configure HistoryPasswordDao.findAll(...).
        final List<HistoryPasswordBO> historyPasswordBOS = Arrays.asList(new HistoryPasswordBO(0L, 0L, "password"));
        when(mockHistoryPasswordDao.findAll()).thenReturn(historyPasswordBOS);

        // Run the test
        final List<HistoryPasswordBO> result = historyPasswordServiceUnderTest.findAll();

        // Verify the results
    }

    @Test
    void testFindAll_HistoryPasswordDaoReturnsNoItems() {
        // Setup
        when(mockHistoryPasswordDao.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final List<HistoryPasswordBO> result = historyPasswordServiceUnderTest.findAll();

        // Verify the results
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testFindById() {
        // Setup
        // Configure HistoryPasswordDao.findById(...).
        final Optional<HistoryPasswordBO> historyPasswordBO = Optional.of(new HistoryPasswordBO(0L, 0L, "password"));
        when(mockHistoryPasswordDao.findById(0L)).thenReturn(historyPasswordBO);

        // Run the test
        final HistoryPasswordBO result = historyPasswordServiceUnderTest.findById(0L);

        // Verify the results
    }

    @Test
    void testFindById_HistoryPasswordDaoReturnsAbsent() {
        // Setup
        when(mockHistoryPasswordDao.findById(0L)).thenReturn(Optional.empty());

        // Run the test
        final HistoryPasswordBO result = historyPasswordServiceUnderTest.findById(0L);

        // Verify the results
        assertThat(result).isNull();
    }

    @Test
    void testGetDatatables() {
        // Setup
        final HistoryPasswordForm historyPasswordForm = new HistoryPasswordForm(0L, 0L, "password", 0, 0);

        // Configure HistoryPasswordDao.getDatatables(...).
        final DataTableResults<HistoryPasswordBean> historyPasswordBeanDataTableResults = new DataTableResults<>();
        historyPasswordBeanDataTableResults.setDraw("draw");
        historyPasswordBeanDataTableResults.setRecordsFiltered("recordsFiltered");
        historyPasswordBeanDataTableResults.setRecordsTotal("recordsTotal");
        historyPasswordBeanDataTableResults.setData(Arrays.asList());
        historyPasswordBeanDataTableResults.setFirst("first");
        when(mockHistoryPasswordDao.getDatatables(any(CommonService.class), any(HistoryPasswordForm.class)))
                .thenReturn(historyPasswordBeanDataTableResults);

        // Run the test
        final DataTableResults<HistoryPasswordBean> result = historyPasswordServiceUnderTest.getDatatables(
                historyPasswordForm);

        // Verify the results
    }

    @Test
    void testSaveOrUpdate() {
        // Setup
        final HistoryPasswordBO entity = new HistoryPasswordBO(0L, 0L, "password");

        // Configure HistoryPasswordDao.save(...).
        final HistoryPasswordBO historyPasswordBO = new HistoryPasswordBO(0L, 0L, "password");
        when(mockHistoryPasswordDao.save(any(HistoryPasswordBO.class))).thenReturn(historyPasswordBO);

        // Run the test
        historyPasswordServiceUnderTest.saveOrUpdate(entity);

        // Verify the results
        verify(mockHistoryPasswordDao).save(any(HistoryPasswordBO.class));
    }

    @Test
    void testDelete() {
        // Setup
        final HistoryPasswordBO entity = new HistoryPasswordBO(0L, 0L, "password");

        // Run the test
        historyPasswordServiceUnderTest.delete(entity);

        // Verify the results
        verify(mockHistoryPasswordDao).delete(any(HistoryPasswordBO.class));
    }

    @Test
    void testSave() {
        // Setup
        final HistoryPasswordForm history = new HistoryPasswordForm(0L, 0L, "password", 0, 0);
        when(mockBcryptEncoder.encode("password")).thenReturn("password");

        // Configure HistoryPasswordDao.save(...).
        final HistoryPasswordBO historyPasswordBO = new HistoryPasswordBO(0L, 0L, "password");
        when(mockHistoryPasswordDao.save(any(HistoryPasswordBO.class))).thenReturn(historyPasswordBO);

        // Run the test
        final HistoryPasswordBO result = historyPasswordServiceUnderTest.save(history);

        // Verify the results
    }

    @Test
    void testFindPassword() {
        // Setup
        // Configure HistoryPasswordDao.findPassword(...).
        final HistoryPasswordBO historyPasswordBO = new HistoryPasswordBO(0L, 0L, "password");
        when(mockHistoryPasswordDao.findPassword(0L)).thenReturn(historyPasswordBO);

        // Run the test
        final HistoryPasswordBO result = historyPasswordServiceUnderTest.findPassword(0L);

        // Verify the results
    }
}
