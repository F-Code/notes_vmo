package com.vmo.notes.attachment.service.impl;

import com.vmo.notes.attachment.bean.AttachmentBean;
import com.vmo.notes.attachment.bo.AttachmentBO;
import com.vmo.notes.attachment.dao.AttachmentDAO;
import com.vmo.notes.attachment.form.AttachmentForm;
import com.vmo.notes.utils.CommonService;
import com.vmo.notes.utils.DataTableResults;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AttachmentServiceTest {

    @Mock
    private AttachmentDAO mockAttachmentDAO;
    @Mock
    private CommonService mockCommonService;

    @InjectMocks
    private AttachmentService attachmentServiceUnderTest;

    @Test
    void testFindAll() {
        // Setup
        // Configure AttachmentDAO.findAll(...).
        final List<AttachmentBO> attachmentBOS = Arrays.asList(
                new AttachmentBO(0L, "name", "path", "type", "mimeType", 0L));
        when(mockAttachmentDAO.findAll()).thenReturn(attachmentBOS);

        // Run the test
        final List<AttachmentBO> result = attachmentServiceUnderTest.findAll();

        // Verify the results
    }

    @Test
    void testFindAll_AttachmentDAOReturnsNoItems() {
        // Setup
        when(mockAttachmentDAO.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final List<AttachmentBO> result = attachmentServiceUnderTest.findAll();

        // Verify the results
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testFindById() {
        // Setup
        // Configure AttachmentDAO.findById(...).
        final Optional<AttachmentBO> attachmentBO = Optional.of(
                new AttachmentBO(0L, "name", "path", "type", "mimeType", 0L));
        when(mockAttachmentDAO.findById(0L)).thenReturn(attachmentBO);

        // Run the test
        final AttachmentBO result = attachmentServiceUnderTest.findById(0L);

        // Verify the results
    }

    @Test
    void testFindById_AttachmentDAOReturnsAbsent() {
        // Setup
        when(mockAttachmentDAO.findById(0L)).thenReturn(Optional.empty());

        // Run the test
        final AttachmentBO result = attachmentServiceUnderTest.findById(0L);

        // Verify the results
        assertThat(result).isNull();
    }

    @Test
    void testGetDatatables() {
        // Setup
        final AttachmentForm attachmentForm = new AttachmentForm(0L, "name", null, "type", 0L, 0, 0);

        // Configure AttachmentDAO.getDatatables(...).
        final DataTableResults<AttachmentBean> attachmentBeanDataTableResults = new DataTableResults<>();
        attachmentBeanDataTableResults.setDraw("draw");
        attachmentBeanDataTableResults.setRecordsFiltered("recordsFiltered");
        attachmentBeanDataTableResults.setRecordsTotal("recordsTotal");
        attachmentBeanDataTableResults.setData(Arrays.asList());
        attachmentBeanDataTableResults.setFirst("first");
        when(mockAttachmentDAO.getDatatables(any(CommonService.class),
                eq(new AttachmentForm(0L, "name", null, "type", 0L, 0, 0)))).thenReturn(attachmentBeanDataTableResults);

        // Run the test
        final DataTableResults<AttachmentBean> result = attachmentServiceUnderTest.getDatatables(attachmentForm);

        // Verify the results
    }

    @Test
    void testSaveOrUpdate() {
        // Setup
        final AttachmentBO entity = new AttachmentBO(0L, "name", "path", "type", "mimeType", 0L);

        // Configure AttachmentDAO.save(...).
        final AttachmentBO attachmentBO = new AttachmentBO(0L, "name", "path", "type", "mimeType", 0L);
        when(mockAttachmentDAO.save(any(AttachmentBO.class))).thenReturn(attachmentBO);

        // Run the test
        attachmentServiceUnderTest.saveOrUpdate(entity);

        // Verify the results
        verify(mockAttachmentDAO).save(any(AttachmentBO.class));
    }

    @Test
    void testDelete() {
        // Setup
        final AttachmentBO entity = new AttachmentBO(0L, "name", "path", "type", "mimeType", 0L);

        // Run the test
        attachmentServiceUnderTest.delete(entity);

        // Verify the results
        verify(mockAttachmentDAO).delete(any(AttachmentBO.class));
    }
}
