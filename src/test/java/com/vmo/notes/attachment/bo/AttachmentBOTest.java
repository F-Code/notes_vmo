package com.vmo.notes.attachment.bo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AttachmentBOTest {

    private AttachmentBO attachmentBOUnderTest;
    private AttachmentBO attachmentBO;

    @BeforeEach
    void setUp() {
        attachmentBOUnderTest = new AttachmentBO(0L, "name", "path", "type", "mimeType", 0L);
        attachmentBO = new AttachmentBO();
    }

    @Test
    void testGetId() {
        Long expected = 0L;
        Long actual = attachmentBOUnderTest.getId();
        assertEquals(expected, actual);
    }

    @Test
    void testSetId() {
        Long expected = 1L;
        attachmentBOUnderTest.setId(1L);
        Long actual = attachmentBOUnderTest.getId();
        assertEquals(expected, actual);
    }

    @Test
    void testGetIdNote() {
        Long expected = 0L;
        Long actual = attachmentBOUnderTest.getIdNote();
        assertEquals(expected, actual);
    }

    @Test
    void testSetIdNote() {
        Long expected = 1L;
        attachmentBOUnderTest.setIdNote(1L);
        Long actual = attachmentBOUnderTest.getIdNote();
        assertEquals(expected, actual);
    }

    @Test
    void testGetName() {
        String expected = "name";
        String actual = attachmentBOUnderTest.getName();
        assertEquals(expected, actual);
    }

    @Test
    void testSetName() {
        String expected = "name Test";
        attachmentBOUnderTest.setName("name Test");
        String actual = attachmentBOUnderTest.getName();
        assertEquals(expected, actual);
    }

    @Test
    void testGetPath() {
        String expected = "path";
        String actual = attachmentBOUnderTest.getPath();
        assertEquals(expected, actual);
    }

    @Test
    void testSetPath() {
        String expected = "path Test";
        attachmentBOUnderTest.setPath("path Test");
        String actual = attachmentBOUnderTest.getPath();
        assertEquals(expected, actual);
    }

    @Test
    void testGetType() {
        String expected = "type";
        String actual = attachmentBOUnderTest.getType();
        assertEquals(expected, actual);
    }

    @Test
    void testSetType() {
        String expected = "type Test";
        attachmentBOUnderTest.setType("type Test");
        String actual = attachmentBOUnderTest.getType();
        assertEquals(expected, actual);
    }

    @Test
    void testMimeType() {
        String expected = "mimeType";
        String actual = attachmentBOUnderTest.getMimeType();
        assertEquals(expected, actual);
    }

    @Test
    void testSetMimeType() {
        String expected = "mimeType Test";
        attachmentBOUnderTest.setMimeType("mimeType Test");
        String actual = attachmentBOUnderTest.getMimeType();
        assertEquals(expected, actual);
    }
}
