package com.vmo.notes.attachment.bean;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AttachmentBeanTest {

    private AttachmentBean attachmentBeanUnderTest;
    private AttachmentBean attachmentBean;

    @BeforeEach
    void setUp() {
        attachmentBeanUnderTest = new AttachmentBean(0L, "name", "path", "type", "mimeType", 0L);
        attachmentBean = new AttachmentBean();
    }

    @Test
    void testGetId() {
        Long expected = 0L;
        Long actual = attachmentBeanUnderTest.getId();
        assertEquals(expected, actual);
    }
    @Test
    void testSetId() {
        Long expected = 1L;
        attachmentBeanUnderTest.setId(1L);
        Long actual = attachmentBeanUnderTest.getId();
        assertEquals(expected, actual);
    }
    @Test
    void testGetIdNote() {
        Long expected = 0L;
        Long actual = attachmentBeanUnderTest.getIdNote();
        assertEquals(expected, actual);
    }
    @Test
    void testSetIdNote() {
        Long expected = 1L;
        attachmentBeanUnderTest.setIdNote(1L);
        Long actual = attachmentBeanUnderTest.getIdNote();
        assertEquals(expected, actual);
    }

    @Test
    void testGetName(){
        String expected = "name";
        String actual = attachmentBeanUnderTest.getName();
        assertEquals(expected,actual);
    }
    @Test
    void testSetName(){
        String expected = "name Test";
        attachmentBeanUnderTest.setName("name Test");
        String actual = attachmentBeanUnderTest.getName();
        assertEquals(expected,actual);
    }
    @Test
    void testGetPath(){
        String expected = "path";
        String actual = attachmentBeanUnderTest.getPath();
        assertEquals(expected,actual);
    }
    @Test
    void testSetPath(){
        String expected = "path Test";
        attachmentBeanUnderTest.setPath("path Test");
        String actual = attachmentBeanUnderTest.getPath();
        assertEquals(expected,actual);
    }

    @Test
    void testGetType(){
        String expected = "type";
        String actual = attachmentBeanUnderTest.getType();
        assertEquals(expected,actual);
    }
    @Test
    void testSetType(){
        String expected = "type Test";
        attachmentBeanUnderTest.setType("type Test");
        String actual = attachmentBeanUnderTest.getType();
        assertEquals(expected,actual);
    }
    @Test
    void testMimeType(){
        String expected = "mimeType";
        String actual = attachmentBeanUnderTest.getMimeType();
        assertEquals(expected,actual);
    }
    @Test
    void testSetMimeType(){
        String expected = "mimeType Test";
        attachmentBeanUnderTest.setMimeType("mimeType Test");
        String actual = attachmentBeanUnderTest.getMimeType();
        assertEquals(expected,actual);
    }
}
