package com.vmo.notes.attachment.controller;

import com.vmo.notes.attachment.bean.AttachmentBean;
import com.vmo.notes.attachment.bo.AttachmentBO;
import com.vmo.notes.attachment.form.AttachmentForm;
import com.vmo.notes.attachment.service.impl.AttachmentService;
import com.vmo.notes.user.bo.UserBO;
import com.vmo.notes.user.service.IUserService;
import com.vmo.notes.utils.DataTableResults;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.security.Principal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class AttachmentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private IUserService mockIUserService;
    @Mock
    private AttachmentService mockAttachmentService;

    @InjectMocks
    private AttachmentController attachmentController;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(attachmentController)
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();
    }

    @Test
    void testFindById() throws Exception {
        // Setup
        // Configure AttachmentService.findById(...).
        final AttachmentBO attachmentBO = new AttachmentBO(0L, "name", "path", "type", "mimeType", 0L);
        when(mockAttachmentService.findById(0L)).thenReturn(attachmentBO);

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(get("/api/attachment/{id}", 0)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).isEqualTo("{\"type\":null,\"code\":\"WARNING\",\"message\":\"record.deleted\",\"data\":null}");
    }

    @Test
    void testProcessSearch() throws Exception {
        // Setup
        // Configure AttachmentService.getDatatables(...).
        final DataTableResults<AttachmentBean> attachmentBeanDataTableResults = new DataTableResults<>();
        attachmentBeanDataTableResults.setDraw("draw");
        attachmentBeanDataTableResults.setRecordsFiltered("recordsFiltered");
        attachmentBeanDataTableResults.setRecordsTotal("recordsTotal");
        attachmentBeanDataTableResults.setData(Arrays.asList());
        attachmentBeanDataTableResults.setFirst("first");
        when(mockAttachmentService.getDatatables(new AttachmentForm(0L, "name", null, "type", 0L, 0, 0)))
                .thenReturn(attachmentBeanDataTableResults);

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(multipart("/api/attachment/get-all")
                        .content("{}").contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).isEqualTo("{\"type\":null,\"code\":\"SUCCESS\",\"message\":\"Get Data List Successfully\",\"data\":null}");
    }

    @Test
    void testSaveOrUpdate() throws Exception {
        // Setup
        // Configure IUserService.findByName(...).
        final UserBO userBO = new UserBO(0L, "username", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber");
        when(mockIUserService.findByName("id")).thenReturn(userBO);
        Principal principal = mock(Principal.class);
        when(principal.getName()).thenReturn("thanhtv");
        MockMultipartFile firstFile = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());
        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(multipart("/api/attachment/path")
                        .file(firstFile)
                        .param("id", "0")
                        .param("name", "name")
                        .param("type", "type")
                        .param("idNote", "0")
                        .param("page", "0")
                        .param("recordPage", "0")
                        .with(user("username"))
                        .principal(principal)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED.value());
    }

    @Test
    void testSaveOrUpdate_ThrowsException() throws Exception {
        // Setup
        // Configure IUserService.findByName(...).
        final UserBO userBO = new UserBO(0L, "username", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber");
        when(mockIUserService.findByName("id")).thenReturn(userBO);
        Principal principal = mock(Principal.class);
        when(principal.getName()).thenReturn("thanhtv");
        // Run the test
        MockMultipartFile firstFile = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());
        final MockHttpServletResponse response = mockMvc.perform(multipart("/api/attachment/path")
                        .file(firstFile)
                        .param("id", "0")
                        .param("name", "name")
                        .param("type", "type")
                        .param("idNote", "0")
                        .param("page", "0")
                        .param("recordPage", "0")
                        .with(user("username"))
                        .principal(principal)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.value());
    }

    @Test
    void testDelete() throws Exception {
        // Setup
        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(delete("/api/attachment/{id}", 0)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).isEqualTo("{\"type\":null,\"code\":\"ERROR\",\"message\":\"error\",\"data\":null}");
    }

    @Test
    void testIndex() throws Exception {
        // Setup
        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(get("/api/attachment/index")
                        .accept(MediaType.TEXT_HTML))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }
}
