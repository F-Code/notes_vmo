package com.vmo.notes.attachment.form;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class AttachmentFormTest {
    private AttachmentForm attachmentFormUnderTest;
    private AttachmentForm attachmentForm;

    @BeforeEach
    void setUp() {
        MockMultipartFile jsonFile = new MockMultipartFile("test.json", "", "application/json", "{\"key1\": \"value1\"}".getBytes());
        attachmentFormUnderTest = new AttachmentForm(0L, "name", jsonFile, "type", 0L, 0, 0);
        attachmentForm = new AttachmentForm();
    }
    @Test
    void testGetId() {
        Long expected = 0L;
        Long actual = attachmentFormUnderTest.getId();
        assertEquals(expected, actual);
    }
    @Test
    void testSetId() {
        Long expected = 1L;
        attachmentFormUnderTest.setId(1L);
        Long actual = attachmentFormUnderTest.getId();
        assertEquals(expected, actual);
    }
    @Test
    void testGetIdNote() {
        Long expected = 0L;
        Long actual = attachmentFormUnderTest.getIdNote();
        assertEquals(expected, actual);
    }
    @Test
    void testSetIdNote() {
        Long expected = 1L;
        attachmentFormUnderTest.setIdNote(1L);
        Long actual = attachmentFormUnderTest.getIdNote();
        assertEquals(expected, actual);
    }

    @Test
    void testGetName(){
        String expected = "name";
        String actual = attachmentFormUnderTest.getName();
        assertEquals(expected,actual);
    }
    @Test
    void testSetName(){
        String expected = "name Test";
        attachmentFormUnderTest.setName("name Test");
        String actual = attachmentFormUnderTest.getName();
        assertEquals(expected,actual);
    }
    @Test
    void testGetImage(){
        String expectedName = "test.json";
        String actual = attachmentFormUnderTest.getImage().getName();
        assertEquals(expectedName,actual);
    }
    @Test
    void testSetImage(){
        String expected = "test1.json";
        MockMultipartFile jsonFile = new MockMultipartFile("test1.json", "", "application/json", "{\"key1\": \"value1\"}".getBytes());
        attachmentFormUnderTest.setImage(jsonFile);
        String actual = attachmentFormUnderTest.getImage().getName();
        assertEquals(expected,actual);
    }
    @Test
    void testGetType(){
        String expected = "type";
        String actual = attachmentFormUnderTest.getType();
        assertEquals(expected,actual);
    }
    @Test
    void testSetType(){
        String expected = "type Test";
        attachmentFormUnderTest.setType("type Test");
        String actual = attachmentFormUnderTest.getType();
        assertEquals(expected,actual);
    }
    @Test
    void testSetPage() {
        int expect = 1;
        this.attachmentFormUnderTest.setPage(1);
        int actual = this.attachmentFormUnderTest.getPage();
        assertEquals(expect, actual);
    }

    @Test
    void  testGetPage() {
        int expect = 0;
        int actual = this.attachmentFormUnderTest.getPage();
        assertEquals(expect, actual);
    }

    @Test
    void testSetRecordPage() {
        int expect = 1;
        this.attachmentFormUnderTest.setRecordPage(1);
        int actual = this.attachmentFormUnderTest.getRecordPage();
        assertEquals(expect, actual);
    }

    @Test
    void  testGetRecordPage() {
        int expect = 0;
        int actual = this.attachmentFormUnderTest.getRecordPage();
        assertEquals(expect, actual);
    }
    
}
