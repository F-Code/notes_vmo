package com.vmo.notes.checkbox.service.impl;

import com.vmo.notes.checkbox.bean.CheckboxBean;
import com.vmo.notes.checkbox.bo.CheckboxBO;
import com.vmo.notes.checkbox.dao.CheckboxDAO;
import com.vmo.notes.checkbox.form.CheckboxForm;
import com.vmo.notes.utils.CommonService;
import com.vmo.notes.utils.DataTableResults;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CheckboxServiceTest {

    @Mock
    private CheckboxDAO mockCheckboxDAO;
    @Mock
    private CommonService mockCommonService;

    @InjectMocks
    private CheckboxService checkboxServiceUnderTest;

    @Test
    void testFindAll() {
        // Setup
        // Configure CheckboxDAO.findAll(...).
        final List<CheckboxBO> checkboxBOS = Arrays.asList(new CheckboxBO(0L, "value", "name", 0L));
        when(mockCheckboxDAO.findAll()).thenReturn(checkboxBOS);

        // Run the test
        final List<CheckboxBO> result = checkboxServiceUnderTest.findAll();

        // Verify the results
    }

    @Test
    void testFindAll_CheckboxDAOReturnsNoItems() {
        // Setup
        when(mockCheckboxDAO.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final List<CheckboxBO> result = checkboxServiceUnderTest.findAll();

        // Verify the results
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testDeleteAllByIdNoteEquals() {
        // Setup
        // Run the test
        checkboxServiceUnderTest.deleteAllByIdNoteEquals(0L);

        // Verify the results
        verify(mockCheckboxDAO).deleteAllByIdNoteEquals(0L);
    }

    @Test
    void testFindById() {
        // Setup
        // Configure CheckboxDAO.findById(...).
        final Optional<CheckboxBO> checkboxBO = Optional.of(new CheckboxBO(0L, "value", "name", 0L));
        when(mockCheckboxDAO.findById(0L)).thenReturn(checkboxBO);

        // Run the test
        final CheckboxBO result = checkboxServiceUnderTest.findById(0L);

        // Verify the results
    }

    @Test
    void testFindById_CheckboxDAOReturnsAbsent() {
        // Setup
        when(mockCheckboxDAO.findById(0L)).thenReturn(Optional.empty());

        // Run the test
        final CheckboxBO result = checkboxServiceUnderTest.findById(0L);

        // Verify the results
        assertThat(result).isNull();
    }

    @Test
    void testGetDatatables() {
        // Setup
        final CheckboxForm checkboxForm = new CheckboxForm(0L, "value", "name", 0L, 0, 0);

        // Configure CheckboxDAO.getDatatables(...).
        final DataTableResults<CheckboxBean> checkboxBeanDataTableResults = new DataTableResults<>();
        checkboxBeanDataTableResults.setDraw("draw");
        checkboxBeanDataTableResults.setRecordsFiltered("recordsFiltered");
        checkboxBeanDataTableResults.setRecordsTotal("recordsTotal");
        checkboxBeanDataTableResults.setData(Arrays.asList());
        checkboxBeanDataTableResults.setFirst("first");
        when(mockCheckboxDAO.getDatatables(any(CommonService.class), any(CheckboxForm.class)))
                .thenReturn(checkboxBeanDataTableResults);

        // Run the test
        final DataTableResults<CheckboxBean> result = checkboxServiceUnderTest.getDatatables(checkboxForm);

        // Verify the results
    }

    @Test
    void testSaveOrUpdate() {
        // Setup
        final CheckboxBO entity = new CheckboxBO(0L, "value", "name", 0L);
        when(mockCheckboxDAO.save(any(CheckboxBO.class))).thenReturn(new CheckboxBO(0L, "value", "name", 0L));

        // Run the test
        checkboxServiceUnderTest.saveOrUpdate(entity);

        // Verify the results
        verify(mockCheckboxDAO).save(any(CheckboxBO.class));
    }

    @Test
    void testDelete() {
        // Setup
        final CheckboxBO entity = new CheckboxBO(0L, "value", "name", 0L);

        // Run the test
        checkboxServiceUnderTest.delete(entity);

        // Verify the results
        verify(mockCheckboxDAO).delete(any(CheckboxBO.class));
    }

    @Test
    void testFindAllByIdNoteEquals() {
        // Setup
        // Configure CheckboxDAO.findAllByIdNoteEquals(...).
        final List<CheckboxBO> checkboxBOS = Arrays.asList(new CheckboxBO(0L, "value", "name", 0L));
        when(mockCheckboxDAO.findAllByIdNoteEquals(0L)).thenReturn(checkboxBOS);

        // Run the test
        final List<CheckboxBO> result = checkboxServiceUnderTest.findAllByIdNoteEquals(0L);

        // Verify the results
    }

    @Test
    void testFindAllByIdNoteEquals_CheckboxDAOReturnsNoItems() {
        // Setup
        when(mockCheckboxDAO.findAllByIdNoteEquals(0L)).thenReturn(Collections.emptyList());

        // Run the test
        final List<CheckboxBO> result = checkboxServiceUnderTest.findAllByIdNoteEquals(0L);

        // Verify the results
        assertThat(result).isEqualTo(Collections.emptyList());
    }
}
