package com.vmo.notes.checkbox.form;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CheckboxFormTest {

    private CheckboxForm checkboxFormUnderTest;
    private CheckboxForm checkboxForm;

    @BeforeEach
    void setUp() {
        checkboxFormUnderTest = new CheckboxForm(0L, "value", "name", 0L, 0, 0);
        checkboxForm = new CheckboxForm();
    }

    @Test
    void testGetId() {
        Long expected = 0L;
        Long actual = checkboxFormUnderTest.getId();
        assertEquals(expected, actual);
    }

    @Test
    void testSetId() {
        Long expected = 1L;
        checkboxFormUnderTest.setId(1L);
        Long actual = checkboxFormUnderTest.getId();
        assertEquals(expected, actual);
    }

    @Test
    void testGetName() {
        String expected = "name";
        String actual = checkboxFormUnderTest.getName();
        assertEquals(expected, actual);
    }

    @Test
    void testSetName() {
        String expected = "name Test";
        checkboxFormUnderTest.setName("name Test");
        String actual = checkboxFormUnderTest.getName();
        assertEquals(expected, actual);
    }

    @Test
    void testGetValue() {
        String expected = "value";
        String actual = checkboxFormUnderTest.getValue();
        assertEquals(expected, actual);
    }

    @Test
    void testSetValue() {
        String expected = "value Test";
        checkboxFormUnderTest.setValue("value Test");
        String actual = checkboxFormUnderTest.getValue();
        assertEquals(expected, actual);
    }

    @Test
    void testGetIdNote() {
        Long expected = 0L;
        Long actual = checkboxFormUnderTest.getIdNote();
        assertEquals(expected, actual);
    }

    @Test
    void testSetIdNote() {
        Long expected = 1L;
        checkboxFormUnderTest.setIdNote(1L);
        Long actual = checkboxFormUnderTest.getIdNote();
        assertEquals(expected, actual);
    }

    @Test
    void testSetPage() {
        int expect = 1;
        this.checkboxFormUnderTest.setPage(1);
        int actual = this.checkboxFormUnderTest.getPage();
        assertEquals(expect, actual);
    }

    @Test
    void testGetPage() {
        int expect = 0;
        int actual = this.checkboxFormUnderTest.getPage();
        assertEquals(expect, actual);
    }

    @Test
    void testSetRecordPage() {
        int expect = 1;
        this.checkboxFormUnderTest.setRecordPage(1);
        int actual = this.checkboxFormUnderTest.getRecordPage();
        assertEquals(expect, actual);
    }

    @Test
    void testGetRecordPage() {
        int expect = 0;
        int actual = this.checkboxFormUnderTest.getRecordPage();
        assertEquals(expect, actual);
    }
}
