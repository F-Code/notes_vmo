package com.vmo.notes.checkbox.controller;

import com.vmo.notes.checkbox.bean.CheckboxBean;
import com.vmo.notes.checkbox.bo.CheckboxBO;
import com.vmo.notes.checkbox.form.CheckboxForm;
import com.vmo.notes.checkbox.service.impl.CheckboxService;
import com.vmo.notes.user.bo.UserBO;
import com.vmo.notes.user.service.impl.UserService;
import com.vmo.notes.utils.DataTableResults;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.security.Principal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class CheckboxControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private UserService mockUserService;
    @Mock
    private CheckboxService mockCheckboxService;
    @InjectMocks
    private CheckboxController checkboxController;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(checkboxController)
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();
    }

    @Test
    void testFindById() throws Exception {
        // Setup
        when(mockCheckboxService.findById(0L)).thenReturn(new CheckboxBO(0L, "value", "name", 0L));

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(get("/api/checkbox/{id}", 0)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).isEqualTo("{\"type\":null,\"code\":\"SUCCESS\",\"message\":\"Get Data Successfully\",\"data\":{\"createdAt\":null,\"createdBy\":null,\"modifiedDate\":null,\"modifiedBy\":null,\"isDeleted\":null,\"id\":0,\"value\":\"value\",\"name\":\"name\",\"idNote\":0}}");
    }

    @Test
    void testProcessSearch() throws Exception {
        // Setup
        // Configure CheckboxService.getDatatables(...).
        final DataTableResults<CheckboxBean> checkboxBeanDataTableResults = new DataTableResults<>();
        checkboxBeanDataTableResults.setDraw("draw");
        checkboxBeanDataTableResults.setRecordsFiltered("recordsFiltered");
        checkboxBeanDataTableResults.setRecordsTotal("recordsTotal");
        checkboxBeanDataTableResults.setData(Arrays.asList());
        checkboxBeanDataTableResults.setFirst("first");
        when(mockCheckboxService.getDatatables(any(CheckboxForm.class))).thenReturn(checkboxBeanDataTableResults);
        Principal principal = mock(Principal.class);
        when(principal.getName()).thenReturn("thanhtv");
        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(post("/api/checkbox/get-all")
                        .content("{}").contentType(MediaType.APPLICATION_JSON)
                        .principal(principal)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).isEqualTo("{\"type\":null,\"code\":\"SUCCESS\",\"message\":\"Get Data List Successfully\",\"data\":{\"draw\":\"draw\",\"first\":\"first\",\"recordsFiltered\":\"recordsFiltered\",\"recordsTotal\":\"recordsTotal\",\"data\":[],\"json\":\"{\\\"draw\\\":\\\"draw\\\",\\\"first\\\":\\\"first\\\",\\\"recordsFiltered\\\":\\\"recordsFiltered\\\",\\\"recordsTotal\\\":\\\"recordsTotal\\\",\\\"data\\\":[]}\"}}");
    }

    @Test
    void testSaveOrUpdate() throws Exception {
        // Setup
        // Configure UserService.findByName(...).
        final UserBO userBO = new UserBO(0L, "username", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber");
        when(mockUserService.findByName("name")).thenReturn(userBO);
        Principal principal = mock(Principal.class);
        when(principal.getName()).thenReturn("thanhtv");
        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(post("/api/checkbox/path")
                        .content("{}").contentType(MediaType.APPLICATION_JSON)
                        .principal(principal)
                        .with(user("username"))
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testDelete() throws Exception {
        // Setup
        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(delete("/api/checkbox/{id}", 0)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).isEqualTo("{\"type\":null,\"code\":\"ERROR\",\"message\":\"error\",\"data\":null}");
    }
}
