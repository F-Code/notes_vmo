package com.vmo.notes.checkbox.bo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CheckboxBOTest {

    private CheckboxBO checkboxBOUnderTest;
    private CheckboxBO checkboxBO;

    @BeforeEach
    void setUp() {
        checkboxBOUnderTest = new CheckboxBO(0L, "value", "name", 0L);
        checkboxBO = new CheckboxBO();
    }
    @Test
    void testGetId() {
        Long expected = 0L;
        Long actual = checkboxBOUnderTest.getId();
        assertEquals(expected, actual);
    }
    @Test
    void testSetId() {
        Long expected = 1L;
        checkboxBOUnderTest.setId(1L);
        Long actual = checkboxBOUnderTest.getId();
        assertEquals(expected, actual);
    }
    @Test
    void testGetName(){
        String expected = "name";
        String actual = checkboxBOUnderTest.getName();
        assertEquals(expected,actual);
    }
    @Test
    void testSetName(){
        String expected = "name Test";
        checkboxBOUnderTest.setName("name Test");
        String actual = checkboxBOUnderTest.getName();
        assertEquals(expected,actual);
    }
    @Test
    void testGetValue(){
        String expected = "value";
        String actual = checkboxBOUnderTest.getValue();
        assertEquals(expected,actual);
    }
    @Test
    void testSetValue(){
        String expected = "value Test";
        checkboxBOUnderTest.setValue("value Test");
        String actual = checkboxBOUnderTest.getValue();
        assertEquals(expected,actual);
    }

    @Test
    void testGetIdNote() {
        Long expected = 0L;
        Long actual = checkboxBOUnderTest.getIdNote();
        assertEquals(expected, actual);
    }
    @Test
    void testSetIdNote() {
        Long expected = 1L;
        checkboxBOUnderTest.setIdNote(1L);
        Long actual = checkboxBOUnderTest.getIdNote();
        assertEquals(expected, actual);
    }
}
