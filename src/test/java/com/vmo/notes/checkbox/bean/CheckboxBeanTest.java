package com.vmo.notes.checkbox.bean;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CheckboxBeanTest {

    private CheckboxBean checkboxBeanUnderTest;
    private CheckboxBean checkboxBean;

    @BeforeEach
    void setUp() {
        checkboxBeanUnderTest = new CheckboxBean(0L, "value", "name", 0L);
        checkboxBean = new CheckboxBean();
    }
    @Test
    void testGetId() {
        Long expected = 0L;
        Long actual = checkboxBeanUnderTest.getId();
        assertEquals(expected, actual);
    }
    @Test
    void testSetId() {
        Long expected = 1L;
        checkboxBeanUnderTest.setId(1L);
        Long actual = checkboxBeanUnderTest.getId();
        assertEquals(expected, actual);
    }
    @Test
    void testGetName(){
        String expected = "name";
        String actual = checkboxBeanUnderTest.getName();
        assertEquals(expected,actual);
    }
    @Test
    void testSetName(){
        String expected = "name Test";
        checkboxBeanUnderTest.setName("name Test");
        String actual = checkboxBeanUnderTest.getName();
        assertEquals(expected,actual);
    }
    @Test
    void testGetValue(){
        String expected = "value";
        String actual = checkboxBeanUnderTest.getValue();
        assertEquals(expected,actual);
    }
    @Test
    void testSetValue(){
        String expected = "value Test";
        checkboxBeanUnderTest.setValue("value Test");
        String actual = checkboxBeanUnderTest.getValue();
        assertEquals(expected,actual);
    }

    @Test
    void testGetIdNote() {
        Long expected = 0L;
        Long actual = checkboxBeanUnderTest.getIdNote();
        assertEquals(expected, actual);
    }
    @Test
    void testSetIdNote() {
        Long expected = 1L;
        checkboxBeanUnderTest.setIdNote(1L);
        Long actual = checkboxBeanUnderTest.getIdNote();
        assertEquals(expected, actual);
    }
}
