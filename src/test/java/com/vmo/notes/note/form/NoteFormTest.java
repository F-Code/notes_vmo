package com.vmo.notes.note.form;

import com.vmo.notes.checkbox.bean.CheckboxBean;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;

import java.util.Collections;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

class NoteFormTest {

    private NoteForm noteFormUnderTest;
    private NoteForm noteForm;

    @BeforeEach
    void setUp() {
        MockMultipartFile firstFile = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());
        noteFormUnderTest = new NoteForm(0L, "name", "url", "description", 0, Collections.singletonList(firstFile),
                Collections.singletonList(new CheckboxBean(0L, "value", "name", 0L)), "createdBy", 0, 0, 0);
        noteForm = new NoteForm();
    }
    @Test
    void testGetId() {
        Long expected = 0L;
        Long actual = noteFormUnderTest.getId();
        assertEquals(expected, actual);
    }

    @Test
    void testSetId() {
        Long expected = 1L;
        noteFormUnderTest.setId(1L);
        Long actual = noteFormUnderTest.getId();
        assertEquals(expected, actual);
    }

    @Test
    void testGetCreatedBy() {
        String expected = "createdBy";
        String actual = noteFormUnderTest.getCreatedBy();
        assertEquals(expected, actual);
    }

    @Test
    void testSetCreatedBy() {
        String expected = "createdBy Test";
        noteFormUnderTest.setCreatedBy("createdBy Test");
        String actual = noteFormUnderTest.getCreatedBy();
        assertEquals(expected, actual);
    }
    @Test
    void testGetName() {
        String expected = "name";
        String actual = noteFormUnderTest.getName();
        assertEquals(expected, actual);
    }

    @Test
    void testSetName() {
        String expected = "name Test";
        noteFormUnderTest.setName("name Test");
        String actual = noteFormUnderTest.getName();
        assertEquals(expected, actual);
    }
    @Test
    void testGetDescription() {
        String expected = "description";
        String actual = noteFormUnderTest.getDescription();
        assertEquals(expected, actual);
    }

    @Test
    void testSetDescription() {
        String expected = "description Test";
        noteFormUnderTest.setDescription("description Test");
        String actual = noteFormUnderTest.getDescription();
        assertEquals(expected, actual);
    }
    @Test
    void testGetUrl() {
        String expected = "url";
        String actual = noteFormUnderTest.getUrl();
        assertEquals(expected, actual);
    }

    @Test
    void testSetUrl() {
        String expected = "url Test";
        noteFormUnderTest.setUrl("url Test");
        String actual = noteFormUnderTest.getUrl();
        assertEquals(expected, actual);
    }
    @Test
    void testGetType() {
        Integer expected = 0;
        Integer actual = noteFormUnderTest.getType();
        assertEquals(expected, actual);
    }

    @Test
    void testSetType() {
        Integer expected = 1;
        noteFormUnderTest.setType(1);
        Integer actual = noteFormUnderTest.getType();
        assertEquals(expected, actual);
    }
    @Test
    void testGetStatus() {
        Integer expected = 0;
        Integer actual = noteFormUnderTest.getStatus();
        assertEquals(expected, actual);
    }

    @Test
    void testSetStaus() {
        Integer expected = 1;
        noteFormUnderTest.setStatus(1);
        Integer actual = noteFormUnderTest.getStatus();
        assertEquals(expected, actual);
    }
    @Test
    void testGetCheckboxList() {
        String expected = "name";
        String actual = noteFormUnderTest.getCheckboxList().get(0).getName();
        assertEquals(expected, actual);
    }

    @Test
    void testSetCheckboxList() {
        String expected = "name Test";
        noteFormUnderTest.setCheckboxList(asList(new CheckboxBean(0L, "value", "name Test", 0L)));
        String actual = noteFormUnderTest.getCheckboxList().get(0).getName();
        assertEquals(expected, actual);
    }
    @Test
    void testGetListImage() {
        String expected = "data";
        String actual = noteFormUnderTest.getImage().get(0).getName();
        assertEquals(expected, actual);
    }

    @Test
    void testSetListImage() {
        MockMultipartFile firstFile = new MockMultipartFile("data Test", "filename.txt", "text/plain", "some xml".getBytes());
        String expected = "data Test";
        noteFormUnderTest.setImage(Collections.singletonList(firstFile));
        String actual = noteFormUnderTest.getImage().get(0).getName();
        assertEquals(expected, actual);
    }

    @Test
    void testSetPage() {
        int expect = 1;
        this.noteFormUnderTest.setPage(1);
        int actual = this.noteFormUnderTest.getPage();
        assertEquals(expect, actual);
    }

    @Test
    void testGetPage() {
        int expect = 0;
        int actual = this.noteFormUnderTest.getPage();
        assertEquals(expect, actual);
    }

    @Test
    void testSetRecordPage() {
        int expect = 1;
        this.noteFormUnderTest.setRecordPage(1);
        int actual = this.noteFormUnderTest.getRecordPage();
        assertEquals(expect, actual);
    }

    @Test
    void testGetRecordPage() {
        int expect = 0;
        int actual = this.noteFormUnderTest.getRecordPage();
        assertEquals(expect, actual);
    }
}
