package com.vmo.notes.note.service.impl;

import com.vmo.notes.checkbox.bean.CheckboxBean;
import com.vmo.notes.note.bean.NoteBean;
import com.vmo.notes.note.bo.NoteBO;
import com.vmo.notes.note.dao.NoteDAO;
import com.vmo.notes.note.form.NoteForm;
import com.vmo.notes.utils.CommonService;
import com.vmo.notes.utils.DataTableResults;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class NoteServiceTest {

    @Mock
    private RestTemplate mockRestTemplate;

    @Mock
    private CommonService mockCommonService;

    @Mock
    private NoteDAO mockNoteDAO;

    @InjectMocks
    private NoteService noteServiceUnderTest;

    @BeforeEach
    void setUp() {
        noteServiceUnderTest = new NoteService(mockRestTemplate,mockCommonService,mockNoteDAO);
    }

    @Test
    void testFindAll() {
        // Setup
        // Run the test
        final List<NoteBO> result = noteServiceUnderTest.findAll();

        // Verify the results
    }

    @Test
    void testFindById() {
        // Setup
        // Run the test
        final NoteBO result = noteServiceUnderTest.findById(0L);

        // Verify the results
    }

    @Test
    void testGetDatatables() {
        // Setup
        final NoteForm noteForm = new NoteForm(0L, "name", "url", "description", 0, Arrays.asList(),
                Arrays.asList(new CheckboxBean(0L, "value", "name", 0L)), "createdBy", 0, 0, 0);

        // Run the test
        final DataTableResults<NoteBean> result = noteServiceUnderTest.getDatatables(noteForm);

        // Verify the results
    }

    @Test
    void testSaveOrUpdate() {
        // Setup
        final NoteBO entity = new NoteBO(0L, "name", "description", 0, 0);

        // Run the test
        noteServiceUnderTest.saveOrUpdate(entity);

        // Verify the results
    }

    @Test
    void testDelete() {
        // Setup
        final NoteBO entity = new NoteBO(0L, "name", "description", 0, 0);

        // Run the test
        noteServiceUnderTest.delete(entity);

        // Verify the results
    }

    @Test
    void testFindTopByOrderByIdDesc() {
        // Setup
        // Run the test
        final NoteBO result = noteServiceUnderTest.findTopByOrderByIdDesc();

        // Verify the results
    }

    @Test
    void testCallAttachment() {
        // Setup
        final MockMultipartFile file = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());
        when(mockRestTemplate.exchange(eq("http://localhost:8888/api/attachment"), eq(HttpMethod.POST),
                eq(new HttpEntity<>(null)), eq(Object.class), any(Object.class)))
                .thenReturn(new ResponseEntity<>("body", HttpStatus.OK));

        // Run the test
        final ResponseEntity result = noteServiceUnderTest.callAttachment(file, 0L, "token");

        // Verify the results
    }

    @Test
    void testCallAttachment_RestTemplateThrowsRestClientException() {
        // Setup
        final MockMultipartFile file = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());
        when(mockRestTemplate.exchange(eq("http://localhost:8888/api/attachment"), eq(HttpMethod.POST),
                eq(new HttpEntity<>(null)), eq(Object.class), any(Object.class))).thenThrow(RestClientException.class);

        // Run the test
//        assertThatThrownBy(() -> noteServiceUnderTest.callAttachment(file, 0L, "token"))
//                .isInstanceOf(RestClientException.class);
    }

    @Test
    void testConvert() {
        // Setup
        final MockMultipartFile firstFile = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());
        final File expectedResult = new File("filename.txt");

        // Run the test
        final File result = NoteService.convert(firstFile);

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
    }
}
