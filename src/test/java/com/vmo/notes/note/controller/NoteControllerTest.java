package com.vmo.notes.note.controller;

import com.vmo.notes.checkbox.bo.CheckboxBO;
import com.vmo.notes.checkbox.service.impl.CheckboxService;
import com.vmo.notes.note.bean.NoteBean;
import com.vmo.notes.note.bo.NoteBO;
import com.vmo.notes.note.form.NoteForm;
import com.vmo.notes.note.service.impl.NoteService;
import com.vmo.notes.user.bo.UserBO;
import com.vmo.notes.user.service.impl.UserService;
import com.vmo.notes.utils.DataTableResults;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class NoteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private UserService mockUserService;
    @Mock
    private NoteService mockNoteService;
    @Mock
    private CheckboxService mockCheckboxService;
    @InjectMocks
    private NoteController mockNoteController;
    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(mockNoteController)
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();
    }
    @Test
    void testFindById() throws Exception {
        // Setup
        // Configure NoteService.findById(...).
        final NoteBO noteBO = new NoteBO(0L, "name", "description", 0, 0);
        when(mockNoteService.findById(0L)).thenReturn(noteBO);

        // Configure CheckboxService.findAllByIdNoteEquals(...).
        final List<CheckboxBO> checkboxBOS = Arrays.asList(new CheckboxBO(0L, "value", "name", 0L));
        when(mockCheckboxService.findAllByIdNoteEquals(0L)).thenReturn(checkboxBOS);

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(get("/api/note/{id}", 0)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testFindById_CheckboxServiceReturnsNoItems() throws Exception {
        // Setup
        // Configure NoteService.findById(...).
        final NoteBO noteBO = new NoteBO(0L, "name", "description", 0, 0);
        when(mockNoteService.findById(0L)).thenReturn(noteBO);

        when(mockCheckboxService.findAllByIdNoteEquals(0L)).thenReturn(Collections.emptyList());

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(get("/api/note/{id}", 0)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testProcessSearch() throws Exception {
        // Setup
        // Configure NoteService.getDatatables(...).
        final DataTableResults<NoteBean> noteBeanDataTableResults = new DataTableResults<>();
        noteBeanDataTableResults.setDraw("draw");
        noteBeanDataTableResults.setRecordsFiltered("recordsFiltered");
        noteBeanDataTableResults.setRecordsTotal("recordsTotal");
        noteBeanDataTableResults.setData(Arrays.asList());
        noteBeanDataTableResults.setFirst("first");
        when(mockNoteService.getDatatables(any(NoteForm.class))).thenReturn(noteBeanDataTableResults);

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(multipart("/api/note/get-all")
                        .content("{content}").contentType(MediaType.APPLICATION_JSON)
                        .with(user("username"))
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testSaveOrUpdate() throws Exception {
        // Setup
        // Configure UserService.findByName(...).
        final UserBO userBO = new UserBO(0L, "username", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber");
        when(mockUserService.findByName("name")).thenReturn(userBO);

        when(mockNoteService.callAttachment(any(MultipartFile.class), eq(0L), eq("")))
                .thenReturn(new ResponseEntity<>("body", HttpStatus.OK));

        // Configure NoteService.findTopByOrderByIdDesc(...).
        final NoteBO noteBO = new NoteBO(0L, "name", "description", 0, 0);
        when(mockNoteService.findTopByOrderByIdDesc()).thenReturn(noteBO);

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(multipart("/api/note/path")
                        .file(new MockMultipartFile("image", "originalFilename", MediaType.APPLICATION_JSON_VALUE,
                                "content".getBytes()))
                        .param("id", "0")
                        .param("name", "name")
                        .param("url", "url")
                        .param("description", "description")
                        .param("type", "0")
                        .param("checkboxList0.id", "0")
                        .param("checkboxList0.value", "value")
                        .param("checkboxList0.name", "name")
                        .param("checkboxList0.idNote", "0")
                        .param("createdBy", "createdBy")
                        .param("status", "0")
                        .param("page", "0")
                        .param("recordPage", "0")
                        .with(user("username"))
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testDelete() throws Exception {
        // Setup
        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(delete("/api/note/{id}", 0)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testReDo() throws Exception {
        // Setup
        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(delete("/api/note/redo/{id}", 0)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testComplete() throws Exception {
        // Setup
        // Configure UserService.findByName(...).
        final UserBO userBO = new UserBO(0L, "username", "firstName", "lastName", "email",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "phoneNumber");
        when(mockUserService.findByName("name")).thenReturn(userBO);

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(post("/api/note/complete/{id}", 0)
                        .with(user("username"))
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }
}
