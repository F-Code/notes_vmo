package com.vmo.notes.note.bean;

import com.vmo.notes.checkbox.bean.CheckboxBean;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static java.util.Arrays.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class NoteBeanTest {

    private NoteBean noteBeanUnderTest;
    private NoteBean noteBean;

    @BeforeEach
    void setUp() {
        noteBeanUnderTest = new NoteBean(0L, "name", "url", "description", 0,
                asList(new CheckboxBean(0L, "value", "name", 0L)), 0);
        noteBean = new NoteBean();
    }
    @Test
    void testGetId() {
        Long expected = 0L;
        Long actual = noteBeanUnderTest.getId();
        assertEquals(expected, actual);
    }

    @Test
    void testSetId() {
        Long expected = 1L;
        noteBeanUnderTest.setId(1L);
        Long actual = noteBeanUnderTest.getId();
        assertEquals(expected, actual);
    }

    @Test
    void testGetName() {
        String expected = "name";
        String actual = noteBeanUnderTest.getName();
        assertEquals(expected, actual);
    }

    @Test
    void testSetName() {
        String expected = "name Test";
        noteBeanUnderTest.setName("name Test");
        String actual = noteBeanUnderTest.getName();
        assertEquals(expected, actual);
    }
    @Test
    void testGetDescription() {
        String expected = "description";
        String actual = noteBeanUnderTest.getDescription();
        assertEquals(expected, actual);
    }

    @Test
    void testSetDescription() {
        String expected = "description Test";
        noteBeanUnderTest.setDescription("description Test");
        String actual = noteBeanUnderTest.getDescription();
        assertEquals(expected, actual);
    }
    @Test
    void testGetUrl() {
        String expected = "url";
        String actual = noteBeanUnderTest.getUrl();
        assertEquals(expected, actual);
    }

    @Test
    void testSetUrl() {
        String expected = "url Test";
        noteBeanUnderTest.setUrl("url Test");
        String actual = noteBeanUnderTest.getUrl();
        assertEquals(expected, actual);
    }
    @Test
    void testGetType() {
        Integer expected = 0;
        Integer actual = noteBeanUnderTest.getType();
        assertEquals(expected, actual);
    }

    @Test
    void testSetType() {
        Integer expected = 1;
        noteBeanUnderTest.setType(1);
        Integer actual = noteBeanUnderTest.getType();
        assertEquals(expected, actual);
    }
    @Test
    void testGetStatus() {
        Integer expected = 0;
        Integer actual = noteBeanUnderTest.getStatus();
        assertEquals(expected, actual);
    }

    @Test
    void testSetStaus() {
        Integer expected = 1;
        noteBeanUnderTest.setStatus(1);
        Integer actual = noteBeanUnderTest.getStatus();
        assertEquals(expected, actual);
    }
    @Test
    void testGetCheckboxList() {
        String expected = "name";
        String actual = noteBeanUnderTest.getCheckboxList().get(0).getName();
        assertEquals(expected, actual);
    }

    @Test
    void testSetCheckboxList() {
        String expected = "name Test";
        noteBeanUnderTest.setCheckboxList(asList(new CheckboxBean(0L, "value", "name Test", 0L)));
        String actual = noteBeanUnderTest.getCheckboxList().get(0).getName();
        assertEquals(expected, actual);
    }

}
