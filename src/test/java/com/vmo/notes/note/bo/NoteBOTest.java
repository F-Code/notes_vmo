package com.vmo.notes.note.bo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NoteBOTest {

    private NoteBO noteBOUnderTest;
    private NoteBO noteBO;

    @BeforeEach
    void setUp() {
        noteBOUnderTest = new NoteBO(0L, "name", "description", 0, 0);
        noteBO = new NoteBO();
    }

    @Test
    void testGetId() {
        Long expected = 0L;
        Long actual = noteBOUnderTest.getId();
        assertEquals(expected, actual);
    }

    @Test
    void testSetId() {
        Long expected = 1L;
        noteBOUnderTest.setId(1L);
        Long actual = noteBOUnderTest.getId();
        assertEquals(expected, actual);
    }

    @Test
    void testGetName() {
        String expected = "name";
        String actual = noteBOUnderTest.getName();
        assertEquals(expected, actual);
    }

    @Test
    void testSetName() {
        String expected = "name Test";
        noteBOUnderTest.setName("name Test");
        String actual = noteBOUnderTest.getName();
        assertEquals(expected, actual);
    }

    @Test
    void testGetDescription() {
        String expected = "description";
        String actual = noteBOUnderTest.getDescription();
        assertEquals(expected, actual);
    }

    @Test
    void testSetDescription() {
        String expected = "description Test";
        noteBOUnderTest.setDescription("description Test");
        String actual = noteBOUnderTest.getDescription();
        assertEquals(expected, actual);
    }

    @Test
    void testGetType() {
        Integer expected = 0;
        Integer actual = noteBOUnderTest.getType();
        assertEquals(expected, actual);
    }

    @Test
    void testSetType() {
        Integer expected = 1;
        noteBOUnderTest.setType(1);
        Integer actual = noteBOUnderTest.getType();
        assertEquals(expected, actual);
    }

    @Test
    void testGetStatus() {
        Integer expected = 0;
        Integer actual = noteBOUnderTest.getStatus();
        assertEquals(expected, actual);
    }

    @Test
    void testSetStaus() {
        Integer expected = 1;
        noteBOUnderTest.setStatus(1);
        Integer actual = noteBOUnderTest.getStatus();
        assertEquals(expected, actual);
    }
}
