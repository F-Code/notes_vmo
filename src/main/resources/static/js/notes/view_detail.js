let editor;
var listFile = [];


// This sample still does not showcase all CKEditor 5 features (!)
// Visit https://ckeditor.com/docs/ckeditor5/latest/features/index.html to browse all the features.

CKEDITOR.ClassicEditor.create(document.getElementById("editor"), {
    // cloudServices: {
    //     // tokenUrl: 'Bearer ' + localStorage.getItem("token"),
    //     uploadUrl: 'http://localhost:8888/api/attachment'
    // },
    // https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html#extended-toolbar-configuration-format
    toolbar: {
        items: ['exportPDF', 'exportWord', '|', 'findAndReplace', 'selectAll', '|', 'heading', '|', 'bold', 'italic', 'strikethrough', 'underline', 'code', 'subscript', 'superscript', 'removeFormat', '|', 'bulletedList', 'numberedList', 'todoList', '|', 'outdent', 'indent', '|', 'undo', 'redo', '-', 'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', 'highlight', '|', 'alignment', '|', 'link', 'insertImage', 'blockQuote', 'insertTable', 'mediaEmbed', 'codeBlock', 'htmlEmbed', '|', 'specialCharacters', 'horizontalLine', 'pageBreak', '|', 'textPartLanguage', '|', 'sourceEditing'],
        shouldNotGroupWhenFull: true
    }, // Changing the language of the interface requires loading the language file using the <script> tag.
    // language: 'es',
    list: {
        properties: {
            styles: true, startIndex: true, reversed: true
        }
    }, // https://ckeditor.com/docs/ckeditor5/latest/features/headings.html#configuration
    heading: {
        options: [{model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph'}, {
            model: 'heading1',
            view: 'h1',
            title: 'Heading 1',
            class: 'ck-heading_heading1'
        }, {model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2'}, {
            model: 'heading3',
            view: 'h3',
            title: 'Heading 3',
            class: 'ck-heading_heading3'
        }, {model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4'}, {
            model: 'heading5',
            view: 'h5',
            title: 'Heading 5',
            class: 'ck-heading_heading5'
        }, {model: 'heading6', view: 'h6', title: 'Heading 6', class: 'ck-heading_heading6'}]
    }, // https://ckeditor.com/docs/ckeditor5/latest/features/editor-placeholder.html#using-the-editor-configuration
    placeholder: 'Welcome to CKEditor 5!', // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-family-feature
    fontFamily: {
        options: ['default', 'Arial, Helvetica, sans-serif', 'Courier New, Courier, monospace', 'Georgia, serif', 'Lucida Sans Unicode, Lucida Grande, sans-serif', 'Tahoma, Geneva, sans-serif', 'Times New Roman, Times, serif', 'Trebuchet MS, Helvetica, sans-serif', 'Verdana, Geneva, sans-serif'],
        supportAllValues: true
    }, // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-size-feature
    fontSize: {
        options: [10, 12, 14, 'default', 18, 20, 22], supportAllValues: true
    }, // Be careful with the setting below. It instructs CKEditor to accept ALL HTML markup.
    // https://ckeditor.com/docs/ckeditor5/latest/features/general-html-support.html#enabling-all-html-features
    htmlSupport: {
        allow: [{
            name: /.*/, attributes: true, classes: true, styles: true
        }]
    }, // Be careful with enabling previews
    // https://ckeditor.com/docs/ckeditor5/latest/features/html-embed.html#content-previews
    htmlEmbed: {
        showPreviews: true
    }, // https://ckeditor.com/docs/ckeditor5/latest/features/link.html#custom-link-attributes-decorators
    link: {
        decorators: {
            addTargetToExternalLinks: true, defaultProtocol: 'https://', toggleDownloadable: {
                mode: 'manual', label: 'Downloadable', attributes: {
                    download: 'file'
                }
            }
        }
    }, // https://ckeditor.com/docs/ckeditor5/latest/features/mentions.html#configuration
    mention: {
        feeds: [{
            marker: '@',
            feed: ['@apple', '@bears', '@brownie', '@cake', '@cake', '@candy', '@canes', '@chocolate', '@cookie', '@cotton', '@cream', '@cupcake', '@danish', '@donut', '@dragée', '@fruitcake', '@gingerbread', '@gummi', '@ice', '@jelly-o', '@liquorice', '@macaroon', '@marzipan', '@oat', '@pie', '@plum', '@pudding', '@sesame', '@snaps', '@soufflé', '@sugar', '@sweet', '@topping', '@wafer'],
            minimumCharacters: 1
        }]
    }, // The "super-build" contains more premium features that require additional configuration, disable them below.
    // Do not turn them on unless you read the documentation and know how to configure them and setup the editor.
    removePlugins: [// These two are commercial, but you can try them out without registering to a trial.
        // 'ExportPdf',
        // 'ExportWord',
        'CKBox', 'CKFinder', 'EasyImage', // This sample uses the Base64UploadAdapter to handle image uploads as it requires no configuration.
        // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/base64-upload-adapter.html
        // Storing images as Base64 is usually a very bad idea.
        // Replace it on production website with other solutions:
        // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/image-upload.html
        // 'Base64UploadAdapter',
        'RealTimeCollaborativeComments', 'RealTimeCollaborativeTrackChanges', 'RealTimeCollaborativeRevisionHistory', 'PresenceList', 'Comments', 'TrackChanges', 'TrackChangesData', 'RevisionHistory', 'Pagination', 'WProofreader', // Careful, with the Mathtype plugin CKEditor will not load when loading this sample
        // from a local file system (file://) - load this site via HTTP server if you enable MathType
        'MathType']
}).then(newEditor => {
    editor = newEditor;
})
    .catch(error => {
        console.error(error);
    });

let db;
let page = 1;
let recordPage = 10;
let totalOfRecord = 0;

function update(number) {
    listFile = [];
    let id = number;
    let name = $('#name').val();

    let page = "";
    let recordPage = "";
    let checkboxes = document.querySelectorAll('input[class="notes"]:checked');
    let check = document.querySelector('#customSwitch1');
    let values = [];
    checkboxes.forEach((checkbox) => {
        values.push({value: checkbox.value, name: checkbox.name});
    });
    const editorData = editor.getData();
    var res = editorData.split('\"');
    var checkFile = 0;
    for (let index = 0; index < res.length; index++) {
        if (res[index].search("data") == 0) {
            listFile.push(res[index]);
        }
        if (res[index].search("http") == 0) {
            checkFile++;
        }
    }
    let type = 1;
    if (check.checked) {
        type = 3;
    } else if (listFile.length > 0 || checkFile > 0) {
        values = [];
        type = 2;
    }
    let jsonObj = {
        id: id,
        name: name,
        status: 0,
        description: editor.getData(),
        type: type,
    };

    var form = new FormData();
    for (var key in jsonObj) {
        form.append(key, jsonObj[key]);
    }
    for (let i = 0; i < values.length; i++) {
        console.log(values[i]);
        form.append("checkboxList[" + i + "].value", values[i].value);
        form.append("checkboxList[" + i + "].name", values[i].name);
    }
    for (let i = 0; i < listFile.length; i++) {
        var file = dataURLtoFile(listFile[i], 'file.');
        form.append("image", file);
    }
    var settings = {
        "url": "/api/note",
        "headers": {
            'Authorization': 'Bearer ' + localStorage.getItem("token")
        },
        "method": "POST",
        "timeout": 0,
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form
    };
    $.ajax(settings).done(function (response) {
        response = JSON.parse(response);
        if (SUCCESS === response.code) {
            success();
            location.reload();
        } else {
            fail("Fail");
        }
    });

    event.preventDefault();
}

function dataURLtoFile(dataurl, filename) {

    var type = "";
    type = dataurl.substr(11, dataurl.search(";") - 11);
    console.log(type);
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1], bstr = atob(arr[1]), n = bstr.length,
        u8arr = new Uint8Array(n);
    while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename + type, {type: mime});
}

function complete(num) {
    let id = num;
    let titleNote = $('#titleNote').val();
    let page = "";
    let recordPage = "";
    let values = [];
    Swal.fire({
        title: 'Do you want to complete?',
        icon: 'question',
        showDenyButton: true,
        confirmButtonText: 'Yes',
        denyButtonText: 'No',
    }).then((result) => {
        if (result.isConfirmed) {
            let department = {
                id: id,
                titleNote: titleNote,
                page: page,
                recordPage: recordPage
            };

            $.ajax({
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem("token")
                },
                type: "POST",
                data: JSON.stringify(department),
                url: "/api/note/complete/" + id,
                success: function (result) {
                    if (SUCCESS === result.code) {
                        success();
                        window.location.href = 'http://localhost:8888/note-page';
                    } else {
                        fail("Fail");
                    }
                }
            });
        } else if (result.isDenied) {
            Swal.fire('Not complete', '', 'info');
        }
    })

    event.preventDefault();
}

$("#customSwitch1").change(function () {
    let check = document.querySelector('#customSwitch1');
    if (check.checked) {
        document.getElementById('list-checkbox').innerHTML = "<div class=\"form-group\">\n" +
            "                                        <label for=\"n1\"> <input type=\"checkbox\" class=\"notes\" name=\"Cook\" value=\"cook-rice\" id=\"n1\">Cook\n" +
            "                                            rice</label>\n" +
            "                                        <label for=\"n2\"><input type=\"checkbox\" class=\"notes\" name=\"Wash clothes\" value=\"wash-clothes\"\n" +
            "                                                               id=\"n2\">\n" +
            "                                            Wash clothes</label>\n" +
            "                                        <label for=\"n3\"><input type=\"checkbox\" class=\"notes\" name=\"To wash dishes\" value=\"wash-dishes\"\n" +
            "                                                               id=\"n3\">To wash dishes</label>\n" +
            "                                    </div>";
    } else {
        document.getElementById('list-checkbox').innerHTML = "";
    }
});
// Process back
$(document).on('click', "#btn-back", function () {
    location.href = "http://localhost:8888/note-page";
});