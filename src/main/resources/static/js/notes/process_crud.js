let editor;
let db;
let page = 1;
let recordPage = 10;
let totalOfRecord = 0;
var listFile = [];

// Process CK editor
CKEDITOR.ClassicEditor.create(document.getElementById("editor"), {
    cloudServices: {
        uploadUrl: 'http://localhost:8888/api/attachment'
    },
    toolbar: {
        items: ['insertImage'],
        shouldNotGroupWhenFull: true
    },
    list: {
        properties: {
            styles: true, startIndex: true, reversed: true
        }
    },
    heading: {
        options: [{model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph'}, {
            model: 'heading1',
            view: 'h1',
            title: 'Heading 1',
            class: 'ck-heading_heading1'
        }, {model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2'}, {
            model: 'heading3',
            view: 'h3',
            title: 'Heading 3',
            class: 'ck-heading_heading3'
        }, {model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4'}, {
            model: 'heading5',
            view: 'h5',
            title: 'Heading 5',
            class: 'ck-heading_heading5'
        }, {model: 'heading6', view: 'h6', title: 'Heading 6', class: 'ck-heading_heading6'}]
    },
    placeholder: 'Welcome to CKEditor!',
    fontFamily: {
        options: ['default', 'Arial, Helvetica, sans-serif', 'Courier New, Courier, monospace', 'Georgia, serif', 'Lucida Sans Unicode, Lucida Grande, sans-serif', 'Tahoma, Geneva, sans-serif', 'Times New Roman, Times, serif', 'Trebuchet MS, Helvetica, sans-serif', 'Verdana, Geneva, sans-serif'],
        supportAllValues: true
    },
    fontSize: {
        options: [10, 12, 14, 'default', 18, 20, 22], supportAllValues: true
    },
    htmlSupport: {
        allow: [{
            name: /.*/, attributes: true, classes: true, styles: true
        }]
    },
    htmlEmbed: {
        showPreviews: true
    },
    link: {
        decorators: {
            addTargetToExternalLinks: true, defaultProtocol: 'https://', toggleDownloadable: {
                mode: 'manual', label: 'Downloadable', attributes: {
                    download: 'file'
                }
            }
        }
    },
    mention: {
        feeds: [{
            marker: '@',
            feed: ['@apple', '@bears', '@brownie', '@cake', '@cake', '@candy', '@canes', '@chocolate', '@cookie', '@cotton', '@cream', '@cupcake', '@danish', '@donut', '@dragée', '@fruitcake', '@gingerbread', '@gummi', '@ice', '@jelly-o', '@liquorice', '@macaroon', '@marzipan', '@oat', '@pie', '@plum', '@pudding', '@sesame', '@snaps', '@soufflé', '@sugar', '@sweet', '@topping', '@wafer'],
            minimumCharacters: 1
        }]
    },
    removePlugins: [
        'CKBox', 'CKFinder', 'EasyImage',
        'RealTimeCollaborativeComments', 'RealTimeCollaborativeTrackChanges', 'RealTimeCollaborativeRevisionHistory', 'PresenceList', 'Comments', 'TrackChanges', 'TrackChangesData', 'RevisionHistory', 'Pagination', 'WProofreader', // Careful, with the Mathtype plugin CKEditor will not load when loading this sample
        'MathType']
}).then(newEditor => {
    editor = newEditor;
})
    .catch(error => {
        console.error(error);
    });

// add new note
function add() {
    listFile = [];
    let id = "";
    let name = $('#name').val();
    let page = "";
    let recordPage = "";
    let checkboxes = document.querySelectorAll('input[class="notes"]:checked');
    let check = document.querySelector('#customSwitch1');
    let values = [];
    checkboxes.forEach((checkbox) => {
        values.push({value: checkbox.value, name: checkbox.name});
    });
    const editorData = editor.getData();
    var res = editorData.split('\"');

    for (let index = 0; index < res.length; index++) {
        if (res[index].search("data") == 0) {
            listFile.push(res[index]);
        }
    }
    let type = 1;
    if (check.checked) {
        type = 3;
    }else if (listFile.length > 0){
        type = 2;
    }
    let jsonObj = {
        id: id,
        name: name,
        description: editor.getData(),
        type: type,
    };

    var form = new FormData();
    for (var key in jsonObj) {
        form.append(key, jsonObj[key]);
    }
    for (let i = 0; i < values.length; i++) {
        console.log(values[i]);
        form.append("checkboxList[" + i + "].value", values[i].value);
        form.append("checkboxList[" + i + "].name", values[i].name);
    }
    for (let i = 0; i < listFile.length; i++) {
        var file = dataURLtoFile(listFile[i], 'file.');
        form.append("image", file);
    }
    var settings = {
        "url": "/api/note",
        "headers": {
            'Authorization': 'Bearer ' + localStorage.getItem("token")
        },
        "method": "POST",
        "timeout": 0,
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form
    };
    $.ajax(settings).done(function (response) {
        response = JSON.parse(response);
        if (SUCCESS === response.code) {
            success();
            location.reload();
        } else {
            fail("Fail");
        }
        window.load();
    });

    event.preventDefault();
}

function dataURLtoFile(dataurl, filename) {

    var type = "";
    type = dataurl.substr(11, dataurl.search(";") - 11);
    console.log(type);
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1], bstr = atob(arr[1]), n = bstr.length,
        u8arr = new Uint8Array(n);
    while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename + type, {type: mime});
}

//update


//load table
function loadInit() {
    console.log("Here");
    let form = {
        page: page,
        recordPage: recordPage
    }
    $.ajax({
        url: "/api/note/get-all",
        type: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem("token")
        },
        data: JSON.stringify(form),
        success: function (result) {
            var html = "";
            var complete = 0;
            var incomplete = 0;
            var htmlDeleteAndRedo = "";
            db = result.data.data;
            totalOfRecord = result.data.recordsTotal;
            $("#page").text(page);
            $("#totalPage").text(Math.ceil(totalOfRecord / recordPage));
            $.each(result.data.data, function (key, item) {
                html += '<tr>';
                html += '<td class="text-nowrap align-middle">' + item.id + '</td>';
                html += '<td class="text-truncate" style="max-width: 120px;">' + item.name + '</td>';
                html += '<td class="text-truncate" style="max-width: 120px;">' + item.description + '</td>';
                html += '<td class="text-truncate" style="max-width: 120px;">' + item.type + '</td>';
                html += '<td class="text-nowrap align-middle">' + formatDate(item.createdAt) + '</td>';
                html += '<td class="text-nowrap align-middle">' + item.createdBy + '</td>';
                html += '<td class="text-nowrap align-middle">' + formatDate(item.modifiedDate) + '</td>'
                html += '<td class="text-nowrap align-middle">' + formatNull(item.modifiedBy) + '</td>';
                html += '<td class="text-nowrap align-middle">' + convertStatus(item.status) + '</td>';
                html += '<td class="text-nowrap align-middle">' + convertIDelete(item.isDeleted) + '</td>';

                html +=
                    `<td>
                          <div class="btn-group align-top">`;
                if (item.status == 1) {
                    complete++;
                } else {
                    incomplete++;
                }
                if (item.isDeleted == 1) {
                    htmlDeleteAndRedo = `<button class="btn btn-sm btn-outline-secondary badge" type="button" onclick='remove(${item.id})'><i class="fa fa-trash"></i></button>`;
                } else {
                    htmlDeleteAndRedo = `<button class="btn btn-sm btn-outline-secondary badge" type="button" onclick='redo(${item.id})'><i class="fa fa-redo"></i></button>`;
                }
                html += htmlDeleteAndRedo;

                html += `<a href='/note/detail/${item.id}' class="btn btn-sm btn-outline-secondary badge" ><i class="fa fa-angle-double-right"></i></a>
                          </div>`;
                html += '</tr>';

                html += `
                    <tr>
                       <td colspan="12" class="hiddenRow">
                            <div class="collapse" id="projects-by${item.id}">                              
                             </div>
                        </td> 
                    </tr>
                `;
            });
            var htmlcomplete = `<b> REPORT: ` + incomplete + ` Incomplete AND ` + complete + ` Complete</b>`;
            $('.tbody').html(html);
            $('.htmlcomplete').html(htmlcomplete);
        }
    });
}

//delete
function remove(id) {
    var url = "/api/note/" + id;
    Swal.fire({
        title: 'Do you want to remove?',
        icon: 'question',
        showDenyButton: true,
        confirmButtonText: 'Yes',
        denyButtonText: 'No',
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                type: "DELETE",
                url: url,
                success: function (result) {
                    if (SUCCESS == result.type) {
                        success();
                        loadInit();
                    } else {
                        fail("Fail");
                    }
                }
            });
        } else if (result.isDenied) {
            Swal.fire('Not save', '', 'info');
        }
    })
    event.preventDefault();
}

function redo(id) {
    var url = "/api/note/redo/" + id;
    Swal.fire({
        title: 'Do you want to redo?',
        icon: 'question',
        showDenyButton: true,
        confirmButtonText: 'Yes',
        denyButtonText: 'No',
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                type: "DELETE",
                url: url,
                success: function (result) {
                    if (SUCCESS == result.type) {
                        success();
                        loadInit();
                    } else {
                        fail("Fail");
                    }
                }
            });
        } else if (result.isDenied) {
            Swal.fire('Not save', '', 'info');
        }
    })
    event.preventDefault();
}


function prev() {
    if (page > 1) {
        page--;
    }
    loadInit();
}

function next() {
    let totalPage = Math.ceil(totalOfRecord / recordPage);
    if (page < totalPage) {
        page++;
    }
    loadInit();
}

function formatDate(dateStr) {
    if (dateStr == null) {
        return "...";
    }
    const date = new Date(dateStr);
    return date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
}

function convertStatus(status) {
    if (status == 1) {
        return "Complete";
    } else {
        return "Incomplete";
    }
}

function convertIDelete(status) {
    if (status == 1) {
        return "Active";
    } else {
        return "Not active";
    }
}

function formatNull(name) {
    if (name == null) {
        return "...";
    } else {
        return name;
    }
}

// Process logout
$(document).on('click', "#btn-logout", function () {
    localStorage.removeItem('token')
    location.href = "http://localhost:8888/login-page";
});

$("#customSwitch1").change(function () {
    let check = document.querySelector('#customSwitch1');
    if (check.checked) {
        document.getElementById('list-checkbox').innerHTML = "<div class=\"form-group\">\n" +
            "                                        <label for=\"n1\"> <input type=\"checkbox\" class=\"notes\" name=\"Cook\" value=\"cook-rice\" id=\"n1\">Cook\n" +
            "                                            rice</label>\n" +
            "                                        <label for=\"n2\"><input type=\"checkbox\" class=\"notes\" name=\"Wash clothes\" value=\"wash-clothes\"\n" +
            "                                                               id=\"n2\">\n" +
            "                                            Wash clothes</label>\n" +
            "                                        <label for=\"n3\"><input type=\"checkbox\" class=\"notes\" name=\"To wash dishes\" value=\"wash-dishes\"\n" +
            "                                                               id=\"n3\">To wash dishes</label>\n" +
            "                                    </div>";
    } else {
        document.getElementById('list-checkbox').innerHTML = "";
    }
});

loadInit();


