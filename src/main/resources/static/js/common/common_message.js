function success() {
    Swal.fire({
        title: 'Success',
        icon: 'success'
    });
}

function fail(mess) {
    Swal.fire({
        title: mess,
        icon: 'error'
    })
}

function notWorking(mess) {
    Swal.fire({
        title: mess + " not working",
        icon: 'info'
    })
}