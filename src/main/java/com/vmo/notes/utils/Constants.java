package com.vmo.notes.utils;

import java.util.Arrays;
import java.util.List;

public class Constants {
    public static class RESPONSE_TYPE {
        public static final String SUCCESS = "SUCCESS";
        public static final String ERROR = "ERROR";
        public static final String WARNING = "WARNING";
        public static final String CONFIRM = "CONFIRM";
    }

    public static class RESPONSE_CODE {
        public static final String SUCCESS = "success";
        public static final String DELETE_SUCCESS = "deleteSuccess";
        public static final String ERROR = "error";
        public static final String WARNING = "warning";
        public static final String RECORD_DELETED = "record.deleted";
    }

    public static class AUTHEN_INFO {
        public static final long ACCESS_TOKEN_VALIDITY_SECONDS = 5 * 60 * 60;
        public static final String SIGNING_KEY = "devglan123r";
        public static final String TOKEN_PREFIX = "Bearer ";
        public static final String HEADER_STRING = "Authorization";
        public static final String AUTHORITIES_KEY = "scopes";
    }

    public static class ACTIVE {
        public static final Integer IS_ACTIVE = 1;
        public static final Integer NOT_ACTICE = 0;

        private ACTIVE() {
        }
    }

    public static class TYPE_OF_NOTE {
        public static final Integer TEXT = 1;
        public static final Integer IMAGE = 2;
        public static final Integer MULTI_CHECKBOX = 3;

        private TYPE_OF_NOTE() {
        }
    }

    public static class METHOD {
        public static final String FIND_BY_ID = "findById";
        public static final String SAVE = "save";
        public static final String UPDATE = "update";
        public static final String DELETE = "delete";
        public static final String GET_ALL = "get all";
    }

    public static class ERROR {
        public static final String string = "is empty";
    }

    public static List<String> LIST_IMAGE_FILE_ACCEPT = Arrays.asList("PNG", "JPG", "JPEG");
}
