package com.vmo.notes.utils;

import javax.transaction.Transactional;
import java.util.List;

public interface ICommonService<BO, Bean, Form> {
    List<BO> findAll();

    BO findById(Long id);

    DataTableResults<Bean> getDatatables(Form form);

    @Transactional
    public void saveOrUpdate(BO entity);

}
