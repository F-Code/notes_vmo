package com.vmo.notes.utils;

import com.vmo.notes.user.bo.UserBO;

import java.util.Date;
import java.util.function.Supplier;

public class CommonCRUD<T extends CommonBO> {
    ICommonService iCommonService;
    Supplier<T> supplier;


    public CommonCRUD(ICommonService iCommonService, Supplier<T> supplier) {
        this.iCommonService = iCommonService;
        this.supplier = supplier;
    }

    public T saveAndUpdate(Long id,UserBO userBO) {
        T t;
        String dateStr = Mixin.convertDateToString(new Date());
        Date date = new Date();
        String idUser = userName(userBO);
        try {
            date = Mixin.convertStringToDate(dateStr);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (id > 0L) {
            t = (T) iCommonService.findById(id);
            if (checkNullAndNotActive(t) == false) {
                return null;
            }
            t.setModifiedDate(date);
            t.setModifiedBy(idUser);
            t.setIsDeleted(Constants.ACTIVE.IS_ACTIVE);
        } else {
            t = supplier.get();
            t.setCreatedAt(date);
            t.setCreatedBy(idUser);
            t.setIsDeleted(Constants.ACTIVE.IS_ACTIVE);
        }
        return t;
    }

    private String userName(UserBO userBO) {
        String name;
        if (userBO != null) {
            name = userBO.getUsername();
        } else {
            name = null;
        }
        return name;
    }

    public Response delete(Long id) {
        if (id > 0L) {
            T t = (T) iCommonService.findById(id);
            if (checkNullAndNotActive(t) == false) {
                return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
            }
            t.setIsDeleted(Constants.ACTIVE.NOT_ACTICE);
            iCommonService.saveOrUpdate(t);
            return Response.success(Constants.RESPONSE_CODE.DELETE_SUCCESS, "Đã xóa");
        } else {
            return Response.error(Constants.RESPONSE_CODE.ERROR);
        }
    }

    public Response redo(Long id) {
        if (id > 0L) {
            T t = (T) iCommonService.findById(id);
            if (t == null) {
                return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
            }
            t.setIsDeleted(Constants.ACTIVE.IS_ACTIVE);
            iCommonService.saveOrUpdate(t);
            return Response.success(Constants.RESPONSE_CODE.SUCCESS, "Redo");
        } else {
            return Response.error(Constants.RESPONSE_CODE.ERROR);
        }
    }

    public Boolean checkNullAndNotActive(T t) {
        if (t == null || t.getIsDeleted() == Constants.ACTIVE.NOT_ACTICE) {
            return false;
        } else {
            return true;
        }
    }
}
