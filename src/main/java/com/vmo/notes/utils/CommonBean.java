package com.vmo.notes.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CommonBean {
    private Date createdAt;

    private String createdBy;

    private Date modifiedDate;

    private String modifiedBy;

    private Integer isDeleted;
}
