package com.vmo.notes.user.controller;

import com.vmo.notes.utils.Constants;
import com.vmo.notes.utils.Response;
import com.vmo.notes.user.bo.UserBO;
import com.vmo.notes.user.form.UserForm;
import com.vmo.notes.user.service.IUserService;
import com.vmo.notes.user.service.impl.UserService;
import com.vmo.notes.userhistorypassword.bo.HistoryPasswordBO;
import com.vmo.notes.userhistorypassword.service.impl.HistoryPasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@CrossOrigin(origins = "*")
@Controller
@RequestMapping(value = "/api")
public class UserController {
    @Autowired
    private HistoryPasswordService historyPasswordService;

    @Autowired
    private IUserService iUserService;

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder bcryptEncoder;

    @GetMapping(path = "/users")
    public List<UserBO> listUser() {
        return iUserService.findAll();
    }

    @PostMapping(path = "/users/get-all")
    public @ResponseBody
    Response processSearch(@RequestBody UserForm form) {
        return Response.success("Get Data List Successfully")
                .withData(userService.getDatatables(form));
    }

    @PostMapping(path = "/signup")
    @ResponseBody
    public Response saveUser(@RequestBody UserForm user) {
        // Validate email
        if (user.getEmail() != null && !user.getEmail().trim().equals("")) {
            String regex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
            Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
            Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(user.getEmail());
            if (!matcher.find()) {
                return Response.custom(Constants.RESPONSE_TYPE.ERROR, "Email " + user.getEmail() + " không hợp lệ");
            }

            // Exist email
            UserBO isExistUser = iUserService.findByEmail(user.getEmail());
            if (isExistUser != null) {
                return Response.error("Email " + user.getEmail() + " đã được đăng ký. Vui lòng thử lại với tài khoản khác");
            }
        }
        HistoryPasswordBO historyPasswordBO = new HistoryPasswordBO();
        historyPasswordBO.setPassword(bcryptEncoder.encode(user.getPassword()));

        UserBO userBO = iUserService.save(user);
        userBO.setIsDeleted(Constants.ACTIVE.IS_ACTIVE);
        userBO.setCreatedAt(new Date());
        userBO.setCreatedBy("admin");

        historyPasswordBO.setUserId(userBO.getId());
        historyPasswordBO.setCreatedAt(new Date());
        historyPasswordBO.setCreatedBy("admin");
        historyPasswordBO.setIsDeleted(Constants.ACTIVE.IS_ACTIVE);
        historyPasswordService.saveOrUpdate(historyPasswordBO);

        return Response.success("Đăng ký account thành công").withData(userBO);
    }

    @GetMapping("/index")
    public String index() {
        return "regis/registation";
    }

}
