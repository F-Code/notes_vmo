package com.vmo.notes.user.controller;

import com.vmo.notes.config.TokenProvider;
import com.vmo.notes.utils.Constants;
import com.vmo.notes.utils.Response;
import com.vmo.notes.user.bean.UserBean;
import com.vmo.notes.user.form.LoginUserForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@CrossOrigin(origins = "*", maxAge = 3600)
@Controller
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenProvider jwtTokenUtil;

    @PostMapping(path = "/login")
    public @ResponseBody
    Response login(@RequestBody LoginUserForm form) throws AuthenticationException {
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        form.getUsername(),
                        form.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String token = jwtTokenUtil.generateToken(authentication);
        UserBean userBean = new UserBean();
        userBean.setToken(token);
        userBean.setAccount(form.getUsername());
        return Response.success(Constants.RESPONSE_TYPE.SUCCESS).withData(userBean);
    }
}
