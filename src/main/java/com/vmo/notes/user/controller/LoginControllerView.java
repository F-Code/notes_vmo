package com.vmo.notes.user.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@CrossOrigin(origins = "*")
@Controller
public class LoginControllerView {

    @GetMapping("/login-page")
    public String index() {
        return "login/login";
    }
}
