package com.vmo.notes.user.service.impl;

import com.vmo.notes.utils.CommonService;
import com.vmo.notes.utils.DataTableResults;
import com.vmo.notes.user.bean.UBean;
import com.vmo.notes.user.bo.UserBO;
import com.vmo.notes.user.dao.UserDAO;
import com.vmo.notes.user.form.UserForm;
import com.vmo.notes.user.service.IUserService;
import com.vmo.notes.userhistorypassword.bo.HistoryPasswordBO;
import com.vmo.notes.userhistorypassword.dao.HistoryPasswordDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service(value = "userService")
public class UserService implements UserDetailsService, IUserService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private CommonService commonService;

    @Autowired
    private HistoryPasswordDao historyPasswordDao;

    @Autowired
    private BCryptPasswordEncoder bcryptEncoder;

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserBO user = userDAO.findByUsername(username);
        HistoryPasswordBO findpass = this.historyPasswordDao.findPassword(user.getId());
        if (user == null || findpass == null) {
            throw new UsernameNotFoundException("Tên tài khoản hoặc mật khẩu không chính xác");
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), findpass.getPassword(), getAuthority(user));
    }

    private Set<SimpleGrantedAuthority> getAuthority(UserBO user) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority("ADMIN"));
        return authorities;
    }

    public List<UserBO> findAll() {
        List<UserBO> list = new ArrayList<>();
        userDAO.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public void delete(long id) {
        userDAO.deleteById(id);
    }

    @Override
    public UserBO findOne(String username) {
        return userDAO.findByUsername(username);
    }

    public DataTableResults<UBean> getDatatables(UserForm userForm) {
        return userDAO.getDatatables(commonService, userForm);
    }
    @Override
    public UserBO findByEmail(String email) {
        return userDAO.findByEmail(email);
    }

    @Override
    public UserBO findById(Long id) {
        return userDAO.findById(id).get();
    }

    @Override
    public UserBO findByName(String name) {
        return userDAO.findByUsername(name);
    }

    @Override
    public UserBO save(UserForm user) {
        UserBO newUser = new UserBO();
        HistoryPasswordBO hisPassword = new HistoryPasswordBO();
        String username = user.getEmail().substring(0, user.getEmail().indexOf('@'));
        newUser.setUsername(username);
        hisPassword.setPassword(bcryptEncoder.encode(user.getPassword()));
        newUser.setEmail(user.getEmail());
        newUser.setFirstName(user.getFirstName());
        newUser.setLastName(user.getLastName());
        newUser.setPhoneNumber(user.getPhoneNumber());
        newUser.setBirthDay(user.getBirthDay());
        return userDAO.save(newUser);
    }

    @Override
    public UserBO updateUserDepartment(UserBO user) {
        return userDAO.save(user);
    }

    @Override
    public List<UserBO> findUBean() {
        return userDAO.findUBeans();
    }
}
