package com.vmo.notes.user.service;

import com.vmo.notes.user.bo.UserBO;
import com.vmo.notes.user.form.UserForm;

import java.util.List;

public interface IUserService {

    UserBO save(UserForm user);

    List<UserBO> findAll();

    void delete(long id);

    UserBO findOne(String username);

    UserBO findByEmail(String email);

    UserBO findById(Long id);

    UserBO findByName(String id);

    UserBO updateUserDepartment(UserBO userBO);

    List<UserBO> findUBean();
}
