package com.vmo.notes.user.dao;

import com.vmo.notes.utils.CommonService;
import com.vmo.notes.utils.DataTableResults;
import com.vmo.notes.utils.Mixin;
import com.vmo.notes.user.bean.UBean;
import com.vmo.notes.user.bo.UserBO;
import com.vmo.notes.user.form.UserForm;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Repository
public interface UserDAO extends CrudRepository<UserBO, Long> {
    UserBO findByUsername(String username);
    UserBO findByEmail(String email);

    default DataTableResults<UBean> getDatatables(CommonService commonService, UserForm formData) {
        List<Object> paramList = new ArrayList<>();

        String sql = " SELECT ";
        sql += "        id As id           ";
        sql += "       ,username As username         ";
        sql += "       ,first_name As firstName        ";
        sql += "       ,last_name As lastName         ";
        sql += "       ,birth_day As birthDay         ";
        sql += "       ,phone_number As phoneNumber         ";
        sql += "       ,created_at As createdAt        ";
        sql += "       ,created_by As createdBy        ";
        sql += "       ,modified_date As modifiedDate        ";
        sql += "       ,modified_by As modifiedBy        ";
        sql += "       ,department_id As departmentId        ";
        sql += "       ,is_deleted As isDeleted        ";
        sql += "       FROM users ";

        StringBuilder strCondition = new StringBuilder(" WHERE 1 = 1 ");

        Mixin.filter(formData.getId(), strCondition, paramList, "id");
        Mixin.filter(formData.getUsername(), strCondition, paramList, "username");
        Mixin.filter(formData.getFirstName(), strCondition, paramList, "first_name");
        Mixin.filter(formData.getLastName(), strCondition, paramList, "last_name");
        Mixin.filter(formData.getBirthDay(), strCondition, paramList, "birth_day");
        Mixin.filter(formData.getPhoneNumber(), strCondition, paramList, "phone_number");
        Mixin.filter(formData.getDepartmentId(), strCondition, paramList, "department_id");

        String orderBy = " ORDER BY id DESC";
        return commonService.findPaginationQueryCustom(sql + strCondition.toString(), orderBy, paramList, UBean.class, formData.getRecordPage(), formData.getPage());
    }

    @Query("select u from UserBO u order by u.id DESC ")
    public List<UserBO> findUBeans();
}
