package com.vmo.notes.user.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// Form login
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginUserForm {
    private String username;

    private String password;
}
