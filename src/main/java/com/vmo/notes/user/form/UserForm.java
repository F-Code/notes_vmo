package com.vmo.notes.user.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserForm {

    private Long id;

    private String username;

    private String password;

    private String firstName;

    private String lastName;

    private String email;

    private Date birthDay;

    private String phoneNumber;

    private Long departmentId;

    private Integer page;

    private Integer recordPage;
}
