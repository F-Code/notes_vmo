package com.vmo.notes.checkbox.dao;

import com.vmo.notes.checkbox.bean.CheckboxBean;
import com.vmo.notes.checkbox.bo.CheckboxBO;
import com.vmo.notes.checkbox.form.CheckboxForm;
import com.vmo.notes.utils.CommonService;
import com.vmo.notes.utils.DataTableResults;
import com.vmo.notes.utils.Mixin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Repository
public interface CheckboxDAO extends CrudRepository<CheckboxBO, Long> {
    /**
     * List all
     */
    List<CheckboxBO> findAll();

    void deleteAllByIdNoteEquals(Long idNote);

    /**
     * findAllByIdNoteEquals
     *
     * @return
     */
    List<CheckboxBO> findAllByIdNoteEquals(Long idNote);

    /**
     * get data by datatable
     *
     * @param commonService
     * @param formData
     * @return
     */
    default DataTableResults<CheckboxBean> getDatatables(CommonService commonService, CheckboxForm formData) {
        List<Object> paramList = new ArrayList<>();

        String sql = " SELECT ";
        sql += "        id As id           ";
        sql += "       ,name As name  ";
        sql += "       ,value As value  ";
        sql += "       FROM checkbox ";

        StringBuilder strCondition = new StringBuilder(" WHERE 1 = 1 ");

        Mixin.filter(formData.getId(), strCondition, paramList, "id");
        Mixin.filter(formData.getName(), strCondition, paramList, "name");
        Mixin.filter(formData.getValue(), strCondition, paramList, "value");

        String orderBy = " ORDER BY id DESC";
        return commonService.findPaginationQueryCustom(sql + strCondition.toString(), orderBy, paramList, CheckboxBean.class, formData.getRecordPage(), formData.getPage());
    }
}
