package com.vmo.notes.checkbox.controller;

import com.vmo.notes.checkbox.bo.CheckboxBO;
import com.vmo.notes.checkbox.form.CheckboxForm;
import com.vmo.notes.checkbox.service.impl.CheckboxService;
import com.vmo.notes.user.bo.UserBO;
import com.vmo.notes.user.service.impl.UserService;
import com.vmo.notes.utils.CommonCRUD;
import com.vmo.notes.utils.Constants;
import com.vmo.notes.utils.ICommonService;
import com.vmo.notes.utils.Mixin;
import com.vmo.notes.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.security.Principal;
import java.util.function.Supplier;

@CrossOrigin("*")
@Controller
@RequestMapping("/api/checkbox")
public class CheckboxController {
    @Autowired
    private UserService userService;
    @Autowired
    private CheckboxService checkboxService;

    private Supplier<CheckboxBO> supplier = new Supplier<CheckboxBO>() {
        @Override
        public CheckboxBO get() {
            return new CheckboxBO();
        }
    };

    /**
     * findById
     *
     * @param id
     * @return
     */
    @GetMapping(path = "/{id}")
    public @ResponseBody
    Response findById(@PathVariable Long id) {
        CommonCRUD<CheckboxBO> commonCRUD = new CommonCRUD((ICommonService) checkboxService, supplier);
        CheckboxBO checkboxBO = checkboxService.findById(id);
        if (commonCRUD.checkNullAndNotActive(checkboxBO) == false) {
            return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
        }
        return Response.success("Get Data Successfully").withData(checkboxBO);
    }

    /**
     * processSearch
     *
     * @param form
     * @return DataTableResults
     */
    @PostMapping(path = "/get-all")
    public @ResponseBody
    Response processSearch(@RequestBody CheckboxForm form) {
        return Response.success("Get Data List Successfully")
                .withData(checkboxService.getDatatables(form));
    }

    /**
     * saveOrUpdate
     *
     * @param form
     * @return
     * @throws Exception
     */
    @PostMapping(path = "")
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    Response saveOrUpdate(@RequestBody CheckboxForm form, Principal principal) {
        CommonCRUD<CheckboxBO> commonCRUD = new CommonCRUD((ICommonService) checkboxService, supplier);
        UserBO userBO = userService.findByName(principal.getName());
        Long id = Mixin.NVL(form.getId());
        CheckboxBO checkboxBO = commonCRUD.saveAndUpdate(id, userBO);
        if (checkboxBO == null) {
            return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
        }
        checkboxBO.setIsDeleted(Constants.ACTIVE.IS_ACTIVE);
        checkboxService.saveOrUpdate(checkboxBO);
        return Response.success(Constants.RESPONSE_CODE.SUCCESS).withData(checkboxBO);
    }

    /**
     * delete
     *
     * @param id
     * @return
     */
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    Response delete(@PathVariable Long id) {
        CommonCRUD<CheckboxBO> commonCRUD = new CommonCRUD((ICommonService) checkboxService, supplier);
        return commonCRUD.delete(id);
    }
}
