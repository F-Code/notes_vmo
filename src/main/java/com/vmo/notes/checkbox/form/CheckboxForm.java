package com.vmo.notes.checkbox.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CheckboxForm {
    private Long id;

    private String value;

    private String name;

    private Long idNote;

    private Integer page;

    private Integer recordPage;
}
