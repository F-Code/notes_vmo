package com.vmo.notes.checkbox.bean;

import com.vmo.notes.utils.CommonBean;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CheckboxBean extends CommonBean {
    private Long id;

    private String value;

    private String name;

    private Long idNote;
}
