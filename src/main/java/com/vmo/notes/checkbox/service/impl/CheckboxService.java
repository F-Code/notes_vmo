package com.vmo.notes.checkbox.service.impl;

import com.vmo.notes.checkbox.bean.CheckboxBean;
import com.vmo.notes.checkbox.bo.CheckboxBO;
import com.vmo.notes.checkbox.dao.CheckboxDAO;
import com.vmo.notes.checkbox.form.CheckboxForm;
import com.vmo.notes.checkbox.service.ICheckboxService;
import com.vmo.notes.utils.CommonService;
import com.vmo.notes.utils.DataTableResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class CheckboxService implements ICheckboxService {
    @Autowired
    private CheckboxDAO checkboxDAO;

    @Autowired
    private CommonService commonService;

    public List<CheckboxBO> findAll() {
        List<CheckboxBO> list = new ArrayList<>();
        checkboxDAO.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    public void deleteAllByIdNoteEquals(Long idNote){
        checkboxDAO.deleteAllByIdNoteEquals(idNote);
    }

    /**
     * findById
     *
     * @param id
     * @return
     */
    public CheckboxBO findById(Long id) {
        return checkboxDAO.findById(id).orElse(null);
    }

    /**
     * getDatatables
     *
     * @param checkboxForm
     * @return
     */
    public DataTableResults<CheckboxBean> getDatatables(CheckboxForm checkboxForm) {
        return checkboxDAO.getDatatables(commonService, checkboxForm);
    }

    /**
     * saveOrUpdate
     *
     * @param entity
     */
    @Transactional
    public void saveOrUpdate(CheckboxBO entity) {
        checkboxDAO.save(entity);
    }

    /**
     * delete
     *
     * @param entity
     */
    public void delete(CheckboxBO entity) {
        checkboxDAO.delete(entity);
    }

    @Override
    public List<CheckboxBO> findAllByIdNoteEquals(Long idNote) {
        return checkboxDAO.findAllByIdNoteEquals(idNote);
    }
}
