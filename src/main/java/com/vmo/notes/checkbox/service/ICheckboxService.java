package com.vmo.notes.checkbox.service;

import com.vmo.notes.checkbox.bean.CheckboxBean;
import com.vmo.notes.checkbox.bo.CheckboxBO;
import com.vmo.notes.checkbox.form.CheckboxForm;
import com.vmo.notes.utils.ICommonService;

import java.util.List;

public interface ICheckboxService extends ICommonService<CheckboxBO, CheckboxBean, CheckboxForm> {
    List<CheckboxBO> findAllByIdNoteEquals(Long idNote);

    void deleteAllByIdNoteEquals(Long idNote);
}
