package com.vmo.notes.note.service.impl;

import com.vmo.notes.note.bean.NoteBean;
import com.vmo.notes.note.bo.NoteBO;
import com.vmo.notes.note.dao.NoteDAO;
import com.vmo.notes.note.form.NoteForm;
import com.vmo.notes.note.service.INoteService;
import com.vmo.notes.utils.CommonService;
import com.vmo.notes.utils.DataTableResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class NoteService implements INoteService {
    @Autowired
    private NoteDAO noteDAO;

    @Autowired
    private CommonService commonService;

    private final RestTemplate restTemplate;

    public NoteService(RestTemplate restTemplate,CommonService commonService,NoteDAO noteDAO) {
        this.restTemplate = restTemplate;
        this.noteDAO = noteDAO;
        this.commonService = commonService;
    }

    public List<NoteBO> findAll() {
        List<NoteBO> list = new ArrayList<>();
        noteDAO.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    /**
     * findById
     *
     * @param id
     * @return
     */
    public NoteBO findById(Long id) {
        return noteDAO.findById(id).orElse(null);
    }

    /**
     * getDatatables
     *
     * @param noteForm
     * @return
     */
    public DataTableResults<NoteBean> getDatatables(NoteForm noteForm) {
        return noteDAO.getDatatables(commonService, noteForm);
    }

    /**
     * saveOrUpdate
     *
     * @param entity
     */
    @Transactional
    public void saveOrUpdate(NoteBO entity) {
        noteDAO.save(entity);
    }

    /**
     * delete
     *
     * @param entity
     */
    public void delete(NoteBO entity) {
        noteDAO.delete(entity);
    }

    @Override
    public NoteBO findTopByOrderByIdDesc() {
        return noteDAO.findTopByOrderByIdDesc();
    }

    public static File convert(MultipartFile file) {
        File convFile = new File(file.getOriginalFilename());
        try {
            convFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(convFile);
            fos.write(file.getBytes());
            fos.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return convFile;
    }

    public ResponseEntity callAttachment(MultipartFile file, Long id, String token) {
        String urlAttchment = "http://localhost:8888/api/attachment";

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ACCEPT, MediaType.ALL_VALUE);
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.add("Authorization", "Bearer " + token);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("image", new FileSystemResource(convert(file)));
        body.add("idTestCaseDetail", id);
        return restTemplate.exchange(urlAttchment, HttpMethod.POST, new HttpEntity(body, headers), Object.class);
    }
}
