package com.vmo.notes.note.service;

import com.vmo.notes.note.bean.NoteBean;
import com.vmo.notes.note.bo.NoteBO;
import com.vmo.notes.note.form.NoteForm;
import com.vmo.notes.utils.ICommonService;

public interface INoteService extends ICommonService<NoteBO, NoteBean, NoteForm> {

    NoteBO findTopByOrderByIdDesc();
}
