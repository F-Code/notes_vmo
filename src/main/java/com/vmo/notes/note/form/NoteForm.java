package com.vmo.notes.note.form;

import com.vmo.notes.checkbox.bean.CheckboxBean;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NoteForm implements Serializable {
    private Long id;

    private String name;

    private String url;

    private String description;

    private Integer type;

    private List<MultipartFile> image;

    private List<CheckboxBean> checkboxList;

    private String createdBy;

    private Integer status;

    private Integer page;

    private Integer recordPage;
}
