package com.vmo.notes.note.dao;

import com.vmo.notes.note.bean.NoteBean;
import com.vmo.notes.note.bo.NoteBO;
import com.vmo.notes.note.form.NoteForm;
import com.vmo.notes.utils.CommonService;
import com.vmo.notes.utils.DataTableResults;
import com.vmo.notes.utils.Mixin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Repository
public interface NoteDAO extends CrudRepository<NoteBO, Long> {
    /**
     * List all
     */
    List<NoteBO> findAll();

    /**
     * get data by datatable
     *
     * @param commonService
     * @param formData
     * @return
     */
    default DataTableResults<NoteBean> getDatatables(CommonService commonService, NoteForm formData) {
        List<Object> paramList = new ArrayList<>();

        String sql = " SELECT ";
        sql += "        id As id           ";
        sql += "       ,name As name  ";
        sql += "       ,description As description  ";
        sql += "       ,type As type  ";
        sql += "       ,created_at As createdAt        ";
        sql += "       ,created_by As createdBy        ";
        sql += "       ,modified_date As modifiedDate        ";
        sql += "       ,modified_by As modifiedBy        ";
        sql += "       ,is_deleted As isDeleted        ";
        sql += "       ,status As status        ";
        sql += "       FROM notes";

        StringBuilder strCondition = new StringBuilder(" WHERE 1 = 1 ");

        Mixin.filter(formData.getId(), strCondition, paramList, "id");
        Mixin.filter(formData.getName(), strCondition, paramList, "name");
        Mixin.filter(formData.getDescription(), strCondition, paramList, "description");
        Mixin.filter(formData.getType(), strCondition, paramList, "type");

        String orderBy = " ORDER BY id DESC";
        return commonService.findPaginationQueryCustom(sql + strCondition.toString(), orderBy, paramList, NoteBean.class, formData.getRecordPage(), formData.getPage());
    }

    NoteBO findTopByOrderByIdDesc();
}
