package com.vmo.notes.note.bean;

import com.vmo.notes.checkbox.bean.CheckboxBean;
import com.vmo.notes.utils.CommonBean;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NoteBean extends CommonBean {
    private Long id;

    private String name;

    private String url;

    private String description;

    private Integer type;

    private List<CheckboxBean> checkboxList;

    private Integer status;
}
