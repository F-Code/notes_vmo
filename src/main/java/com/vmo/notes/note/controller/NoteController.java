package com.vmo.notes.note.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vmo.notes.attachment.bean.AttachmentBean;
import com.vmo.notes.checkbox.bean.CheckboxBean;
import com.vmo.notes.checkbox.bo.CheckboxBO;
import com.vmo.notes.checkbox.service.impl.CheckboxService;
import com.vmo.notes.note.bean.NoteBean;
import com.vmo.notes.note.bo.NoteBO;
import com.vmo.notes.note.form.NoteForm;
import com.vmo.notes.note.service.impl.NoteService;
import com.vmo.notes.user.bo.UserBO;
import com.vmo.notes.user.service.impl.UserService;
import com.vmo.notes.utils.CommonCRUD;
import com.vmo.notes.utils.Constants;
import com.vmo.notes.utils.ICommonService;
import com.vmo.notes.utils.Mixin;
import com.vmo.notes.utils.Response;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import static javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA;

@CrossOrigin("*")
@Controller
@RequestMapping("/api/note")
public class NoteController {
    @Autowired
    private UserService userService;

    @Autowired
    private NoteService noteService;

    @Autowired
    private CheckboxService checkboxService;

    private Supplier<NoteBO> supplier = new Supplier<NoteBO>() {
        @Override
        public NoteBO get() {
            return new NoteBO();
        }
    };

    private Supplier<CheckboxBO> supplierCheckboxBO = new Supplier<CheckboxBO>() {
        @Override
        public CheckboxBO get() {
            return new CheckboxBO();
        }
    };

    /**
     * findById
     *
     * @param id
     * @return
     */
    @GetMapping(path = "/{id}")
    public @ResponseBody
    Response findById(@PathVariable Long id) {
        CommonCRUD<NoteBO> commonCRUD = new CommonCRUD((ICommonService) noteService, supplier);
        NoteBO noteBO = noteService.findById(id);
        if (commonCRUD.checkNullAndNotActive(noteBO) == false) {
            return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
        }
        List<CheckboxBO> checkboxServiceAllByIdNoteEquals = checkboxService.findAllByIdNoteEquals(id);
        NoteBean noteBean = new NoteBean();
        BeanUtils.copyProperties(noteBO, noteBean);
        List<CheckboxBean> checkboxBeanServiceAllByIdNoteEquals = new ArrayList<>();
        for (CheckboxBO checkboxBO : checkboxServiceAllByIdNoteEquals) {
            CheckboxBean checkboxBean = new CheckboxBean();
            BeanUtils.copyProperties(checkboxBO, checkboxBean);
            checkboxBeanServiceAllByIdNoteEquals.add(checkboxBean);
        }
        noteBean.setCheckboxList(checkboxBeanServiceAllByIdNoteEquals);
        return Response.success("Get Data Successfully").withData(noteBO);
    }

    /**
     * processSearch
     *
     * @param form
     * @return DataTableResults
     */
    @PostMapping(path = "/get-all")
    public @ResponseBody
    Response processSearch(@RequestBody NoteForm form, Principal principal) {
        form.setCreatedBy(principal.getName());
        return Response.success("Get Data List Successfully")
                .withData(noteService.getDatatables(form));
    }

    /**
     * saveOrUpdate
     *
     * @param form
     * @return
     * @throws Exception
     */
    @PostMapping(path = "", consumes = MULTIPART_FORM_DATA)
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    Response saveOrUpdate(@ModelAttribute NoteForm form, Principal principal) {
        String token = "";
        List<AttachmentBean> listPath = new ArrayList<>();
        List<String> listBase64 = new ArrayList<>();
        CommonCRUD<NoteBO> commonCRUD = new CommonCRUD((ICommonService) noteService, supplier);
        UserBO userBO = userService.findByName(principal.getName());
        Long id = Mixin.NVL(form.getId());
        NoteBO noteBO = commonCRUD.saveAndUpdate(id, userBO);
        if (noteBO == null) {
            return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
        }
        noteBO.setIsDeleted(Constants.ACTIVE.IS_ACTIVE);
        noteBO.setType(form.getType());
        noteBO.setDescription(form.getDescription());
        noteBO.setName(form.getName());
        noteBO.setStatus(form.getStatus() != null ? form.getStatus() : Constants.ACTIVE.NOT_ACTICE);
        noteService.saveOrUpdate(noteBO);
        RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
        if (attributes != null) {
            token = ((ServletRequestAttributes) attributes).getRequest()
                    .getHeader("Authorization");
        }
        if (form.getImage() != null) {
            ObjectMapper mapper = new ObjectMapper();
            for (int i = 0; i < form.getImage().size(); i++) {
                ResponseEntity responseEntity = noteService.callAttachment(form.getImage().get(i), noteBO.getId(), token);
                Object o = responseEntity.getBody();
                Response response = mapper.convertValue(o, Response.class);
                AttachmentBean bean = mapper.convertValue(response.getData(), AttachmentBean.class);
                listPath.add(bean);
            }
        }
        String description = form.getDescription();
        char regex = '"';
        String split[] = form.getDescription().split(String.valueOf(regex));
        for (int index = 0; index < split.length; index++) {
            if (split[index].indexOf("data") == 0) {
                listBase64.add(split[index]);
            }
        }
        for (AttachmentBean a : listPath) {
            for (String base64 : listBase64) {
                if (listBase64.indexOf(base64) == listPath.indexOf(a)) {
                    description = description.replace(base64, a.getPath());
                }
            }
        }
        noteBO.setDescription(description);
        if (form.getType().equals(Constants.TYPE_OF_NOTE.IMAGE)) {
            checkboxService.deleteAllByIdNoteEquals(form.getId());
        }
        if (form.getType().equals(Constants.TYPE_OF_NOTE.MULTI_CHECKBOX)) {
            noteBO = noteService.findTopByOrderByIdDesc();
            List<CheckboxBean> checkboxBeanList = form.getCheckboxList();
            CheckboxBO checkboxBO;
            checkboxService.deleteAllByIdNoteEquals(form.getId());
            for (CheckboxBean checkboxBean : checkboxBeanList) {
                CommonCRUD commonCRUDNew = new CommonCRUD((ICommonService) checkboxService, supplierCheckboxBO);
                checkboxBO = (CheckboxBO) commonCRUDNew.saveAndUpdate(id, userBO);
                checkboxBO.setId(checkboxBean.getId());
                checkboxBO.setValue(checkboxBean.getValue());
                checkboxBO.setName(checkboxBean.getName());
                checkboxBO.setIdNote(form.getId() == null ? noteBO.getId() : form.getId());
                checkboxService.saveOrUpdate(checkboxBO);
            }
        }
        return Response.success(Constants.RESPONSE_CODE.SUCCESS).withData(noteBO);
    }

    /**
     * delete
     *
     * @param id
     * @return
     */
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    Response delete(@PathVariable Long id) {
        CommonCRUD<NoteBO> commonCRUD = new CommonCRUD((ICommonService) noteService, supplier);
        return commonCRUD.delete(id);
    }

    /**
     * redo
     *
     * @param id
     * @return
     */
    @DeleteMapping(path = "/redo/{id}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    Response reDo(@PathVariable Long id) {
        CommonCRUD<NoteBO> commonCRUD = new CommonCRUD((ICommonService) noteService, supplier);
        Response response = commonCRUD.redo(id);
        return response;
    }

    @PostMapping(path = "/complete/{id}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    Response complete(@PathVariable Long id, Principal principal) {
        CommonCRUD<NoteBO> commonCRUD = new CommonCRUD((ICommonService) noteService, supplier);
        UserBO userBO = userService.findByName(principal.getName());
        NoteBO noteBO = commonCRUD.saveAndUpdate(id, userBO);
        if (noteBO == null) {
            return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
        }
        noteBO.setStatus(Constants.ACTIVE.IS_ACTIVE);
        noteService.saveOrUpdate(noteBO);
        return Response.success(Constants.RESPONSE_CODE.SUCCESS).withData(noteBO);
    }
}
