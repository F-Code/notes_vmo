package com.vmo.notes.note.controller;

import com.vmo.notes.checkbox.bean.CheckboxBean;
import com.vmo.notes.checkbox.bo.CheckboxBO;
import com.vmo.notes.checkbox.service.impl.CheckboxService;
import com.vmo.notes.note.bean.NoteBean;
import com.vmo.notes.note.bo.NoteBO;
import com.vmo.notes.note.form.NoteForm;
import com.vmo.notes.note.service.impl.NoteService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin("*")
@Controller
@RequestMapping
public class NoteControllerView {

    @Autowired
    private NoteService noteService;

    @Autowired
    private CheckboxService checkboxService;

    @GetMapping("/note-page")
    public String index() {
        return "notes/crud";
    }

    @GetMapping("/note/detail/{id}")
    public String detail(Model model, @PathVariable Long id) {
        String pattern = "EEEE dd-MM-yyy HH:mm:ss aa";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        NoteForm noteForm = new NoteForm();
        noteForm.setId(id);
        NoteBO noteBO = this.noteService.findById(id);

        List<CheckboxBO> checkboxServiceAllByIdNoteEquals = checkboxService.findAllByIdNoteEquals(id);
        NoteBean noteBean = new NoteBean();
        BeanUtils.copyProperties(noteBO, noteBean);
        List<CheckboxBean> checkboxBeanServiceAllByIdNoteEquals = new ArrayList<>();
        for (CheckboxBO checkboxBO : checkboxServiceAllByIdNoteEquals) {
            CheckboxBean checkboxBean = new CheckboxBean();
            BeanUtils.copyProperties(checkboxBO, checkboxBean);
            checkboxBeanServiceAllByIdNoteEquals.add(checkboxBean);
        }
        noteBean.setCheckboxList(checkboxBeanServiceAllByIdNoteEquals);
        List<CheckboxBO> listNote = new ArrayList<>();
        listNote.add(new CheckboxBO(null, "cook-rice", "Cook rice", null));
        listNote.add(new CheckboxBO(null, "wash-clothes", "Wash clothes", null));
        listNote.add(new CheckboxBO(null, "wash-dishes", "To wash dishes", null));
        List<CheckboxBO> listNoteNull = new ArrayList<>();
        int check = 0;
        for (int i = 0; i < listNote.size(); i++) {
            check = 0;
            for (int j = 0; j < checkboxServiceAllByIdNoteEquals.size(); j++) {
                if (checkboxServiceAllByIdNoteEquals.get(j).getValue().equals(listNote.get(i).getValue())) {
                    check++;
                }
            }
            if (check == 0) {
                listNoteNull.add(listNote.get(i));
            }
        }
        noteBean.setCheckboxList(checkboxBeanServiceAllByIdNoteEquals);
        model.addAttribute("item", noteBean);
        model.addAttribute("simpleDateFormat", simpleDateFormat);
        model.addAttribute("listNotes", listNote);
        model.addAttribute("listMultiCheckbox", checkboxServiceAllByIdNoteEquals);
        model.addAttribute("listNoteNull", listNoteNull);
        return "notes/detail";
    }
}
