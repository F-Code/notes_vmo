package com.vmo.notes.attachment.dao;

import com.vmo.notes.attachment.bean.AttachmentBean;
import com.vmo.notes.attachment.bo.AttachmentBO;
import com.vmo.notes.attachment.form.AttachmentForm;
import com.vmo.notes.utils.CommonService;
import com.vmo.notes.utils.DataTableResults;
import com.vmo.notes.utils.Mixin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Repository
public interface AttachmentDAO extends CrudRepository<AttachmentBO, Long> {
    /**
     * List all
     */
    List<AttachmentBO> findAll();

    /**
     * get data by datatable
     *
     * @param commonService
     * @param formData
     * @return
     */
    default DataTableResults<AttachmentBean> getDatatables(CommonService commonService, AttachmentForm formData) {
        List<Object> paramList = new ArrayList<>();

        String sql = " SELECT ";
        sql += "        id As id           ";
        sql += "       ,type As type  ";
        sql += "       ,name As name        ";
        sql += "       ,path As path         ";
        sql += "       ,created_at As createdAt        ";
        sql += "       ,created_by As createdBy        ";
        sql += "       ,modified_date As modifiedDate        ";
        sql += "       ,modified_by As modifiedBy        ";
        sql += "       ,is_Deleted As isDeleted        ";
        sql += "       ,id_testcase_detail As IdTestCaseDetail         ";
        sql += "       FROM attachments ";

        StringBuilder strCondition = new StringBuilder(" WHERE 1 = 1 ");

        Mixin.filter(formData.getId(), strCondition, paramList, "id");
        Mixin.filter(formData.getName(), strCondition, paramList, "type");
        Mixin.filter(formData.getType(), strCondition, paramList, "name");
        Mixin.filter(formData.getType(), strCondition, paramList, "idTestCaseDetail");

        String orderBy = " ORDER BY id DESC";
        return commonService.findPaginationQueryCustom(sql + strCondition.toString(), orderBy, paramList, AttachmentBean.class, formData.getRecordPage(), formData.getPage());
    }
}
