package com.vmo.notes.attachment.controller;

import com.vmo.notes.attachment.bo.AttachmentBO;
import com.vmo.notes.attachment.form.AttachmentForm;
import com.vmo.notes.attachment.service.impl.AttachmentService;
import com.vmo.notes.user.bo.UserBO;
import com.vmo.notes.user.service.IUserService;
import com.vmo.notes.utils.CommonCRUD;
import com.vmo.notes.utils.Constants;
import com.vmo.notes.utils.ICommonService;
import com.vmo.notes.utils.Mixin;
import com.vmo.notes.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.security.Principal;
import java.util.Date;
import java.util.function.Supplier;

import static javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA;

@CrossOrigin("*")
@Controller
@RequestMapping("/api/attachment")
public class AttachmentController {
    @Autowired
    private IUserService IUserService;

    @Autowired
    private AttachmentService attachmentService;

    private final static String OBJECT = "Department";

    private Supplier<AttachmentBO> supplier = new Supplier<AttachmentBO>() {
        @Override
        public AttachmentBO get() {
            return new AttachmentBO();
        }
    };

    /**
     * findById
     *
     * @param id
     * @return
     */
    @GetMapping(path = "/{id}")
    public @ResponseBody
    Response findById(@PathVariable Long id) {
        CommonCRUD<AttachmentBO> commonCRUD = new CommonCRUD((ICommonService) attachmentService, supplier);
        AttachmentBO navbarsBO = attachmentService.findById(id);
        if (commonCRUD.checkNullAndNotActive(navbarsBO)) {
            return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
        }
        return Response.success("Lấy chi tiết thành công").withData(navbarsBO);
    }

    /**
     * processSearch
     *
     * @param form
     * @return DataTableResults
     */
    @PostMapping(path = "/get-all")
    public @ResponseBody
    Response processSearch(@RequestBody AttachmentForm form) {
        return Response.success("Get Data List Successfully").withData(attachmentService.getDatatables(form));
    }

    /**
     * saveOrUpdate
     *
     * @param form
     * @return
     * @throws Exception
     */
    @PostMapping(path = "", consumes = MULTIPART_FORM_DATA)
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    Response saveOrUpdate(@ModelAttribute AttachmentForm form, Principal principal) throws Exception {
        Long id = Mixin.NVL(form.getId());
        CommonCRUD<AttachmentBO> commonCRUD = new CommonCRUD((ICommonService) attachmentService, supplier);
        UserBO userBO = IUserService.findByName(principal.getName());
        AttachmentBO attachmentBO = commonCRUD.saveAndUpdate(id, userBO);
        if (attachmentBO == null) {
            return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
        }

        String originalFilename = Mixin.removeSignAndMultiSpace(form.getImage().getOriginalFilename());
        String[] split = originalFilename.split("\\.");
        String extension = "";
        String fileName = "";

        // get extension and filename
        if (split.length > 0) {
            fileName = split[0];
            extension = split[split.length - 1];
        }

        boolean imgFileAccept = Constants.LIST_IMAGE_FILE_ACCEPT.contains(extension.toUpperCase()) == true ? true : false;
        if (!imgFileAccept) {
            return Response.custom(Constants.RESPONSE_CODE.ERROR, "Vui lòng chỉ upload ảnh có định dạng PNG, JPG, JPEG");
        }

        String currentTimeStamp = Mixin.convertDateToString(new Date(), "yyyy_MM_dd_HH_mm_ss");

        String fileNameAttach = Mixin.removeSignAndMultiSpace(fileName) + "_" + currentTimeStamp + "." + extension;
        attachmentBO.setName(fileNameAttach);

        attachmentBO.setType(extension.toUpperCase());

        String link = "";
        String mimeType = "";
        link = Mixin.getImgurContent(form.getImage());
        mimeType = "image/" + extension.toLowerCase();
        attachmentBO.setPath(link);
        attachmentBO.setMimeType(mimeType);
        attachmentBO.setIdNote(form.getIdNote());
        attachmentService.saveOrUpdate(attachmentBO);
        return Response.success(Constants.RESPONSE_CODE.SUCCESS).withData(attachmentBO);
    }

    /**
     * delete
     *
     * @param id
     * @return
     */
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    Response delete(@PathVariable Long id) {
        CommonCRUD<AttachmentBO> commonCRUD = new CommonCRUD((ICommonService) attachmentService, supplier);
        Response response = commonCRUD.delete(id);
        return response;
    }

    @GetMapping("/index")
    public String index() {
        return "attachment/list";
    }
}
