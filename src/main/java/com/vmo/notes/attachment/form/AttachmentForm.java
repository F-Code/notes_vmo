package com.vmo.notes.attachment.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AttachmentForm implements Serializable {
    private Long id;

    private String name;

    private MultipartFile image;

    private String type;

    private Long idNote;

    private Integer page;

    private Integer recordPage;
}
