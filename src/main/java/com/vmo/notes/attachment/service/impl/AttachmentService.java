

package com.vmo.notes.attachment.service.impl;

import com.vmo.notes.attachment.bean.AttachmentBean;
import com.vmo.notes.attachment.bo.AttachmentBO;
import com.vmo.notes.attachment.dao.AttachmentDAO;
import com.vmo.notes.attachment.form.AttachmentForm;
import com.vmo.notes.attachment.service.IAttachmentService;
import com.vmo.notes.utils.CommonService;
import com.vmo.notes.utils.DataTableResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class AttachmentService implements IAttachmentService {
    @Autowired
    private AttachmentDAO attachmentDAO;

    @Autowired
    private CommonService commonService;

    public List<AttachmentBO> findAll() {
        List<AttachmentBO> list = new ArrayList<>();
        attachmentDAO.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    /**
     * findById
     *
     * @param id
     * @return
     */
    public AttachmentBO findById(Long id) {
        return attachmentDAO.findById(id).orElse(null);
    }

    /**
     * getDatatables
     *
     * @param attachmentForm
     * @return
     */
    public DataTableResults<AttachmentBean> getDatatables(AttachmentForm attachmentForm) {
        return attachmentDAO.getDatatables(commonService, attachmentForm);
    }

    /**
     * saveOrUpdate
     *
     * @param entity
     */
    @Transactional
    public void saveOrUpdate(AttachmentBO entity) {
        attachmentDAO.save(entity);
    }

    /**
     * delete
     *
     * @param entity
     */
    public void delete(AttachmentBO entity) {
        attachmentDAO.delete(entity);
    }
}
