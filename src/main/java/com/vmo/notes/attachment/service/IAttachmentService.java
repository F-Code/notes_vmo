package com.vmo.notes.attachment.service;

import com.vmo.notes.attachment.bean.AttachmentBean;
import com.vmo.notes.attachment.bo.AttachmentBO;
import com.vmo.notes.attachment.form.AttachmentForm;
import com.vmo.notes.utils.ICommonService;

public interface IAttachmentService extends ICommonService<AttachmentBO, AttachmentBean, AttachmentForm> {
}
