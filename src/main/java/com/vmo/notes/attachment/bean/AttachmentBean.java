
package com.vmo.notes.attachment.bean;

import com.vmo.notes.utils.CommonBean;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AttachmentBean extends CommonBean {
    private Long id;

    private String name;

    private String path;

    private String type;

    private String mimeType;

    private Long idNote;
}
