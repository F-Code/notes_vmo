package com.vmo.notes.userhistorypassword.dao;

import com.vmo.notes.utils.CommonService;
import com.vmo.notes.utils.DataTableResults;
import com.vmo.notes.utils.Mixin;
import com.vmo.notes.userhistorypassword.bean.HistoryPasswordBean;
import com.vmo.notes.userhistorypassword.bo.HistoryPasswordBO;
import com.vmo.notes.userhistorypassword.form.HistoryPasswordForm;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Repository
public interface HistoryPasswordDao extends CrudRepository<HistoryPasswordBO,Long> {
    /**
     * List all
     */
    public List<HistoryPasswordBO> findAll();
    HistoryPasswordBO findByUserId(long id);
    @Query(value = "SELECT * from history_password WHERE user_id = ?1 order by created_at DESC  LIMIT 1", nativeQuery = true)
    HistoryPasswordBO findPassword(Long idUser);

    /**
     * get data by datatable
     *
     * @param commonService
     * @param formData
     * @return
     */
    default DataTableResults<HistoryPasswordBean> getDatatables(CommonService commonService, HistoryPasswordForm formData) {
        List<Object> paramList = new ArrayList<>();

        String sql = " SELECT ";
        sql += "        id As id           ";
        sql += "       ,user_id As userId         ";
        sql += "       ,password As password        ";
        sql += "       ,created_at As createAt        ";
        sql += "       ,created_by As createBy        ";
        sql += "       ,modified_date As modifiedDate        ";
        sql += "       ,modified_by As modifiedBy        ";
        sql += "       ,is_Deleted As isDeleted        ";
        sql += "       FROM history_password ";

        StringBuilder strCondition = new StringBuilder(" WHERE 1 = 1 ");

       Mixin.filter(formData.getId(), strCondition, paramList, "id");
        Mixin.filter(formData.getUserId(), strCondition, paramList, "user_id");
        Mixin.filter(formData.getPassword(), strCondition, paramList, "password");

        String orderBy = " ORDER BY id DESC";
        return commonService.findPaginationQueryCustom(sql + strCondition.toString(), orderBy, paramList, HistoryPasswordBean.class, formData.getRecordPage(), formData.getPage());
    }
}
