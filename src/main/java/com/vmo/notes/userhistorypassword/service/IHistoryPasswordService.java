package com.vmo.notes.userhistorypassword.service;

import com.vmo.notes.utils.ICommonService;
import com.vmo.notes.userhistorypassword.bean.HistoryPasswordBean;
import com.vmo.notes.userhistorypassword.bo.HistoryPasswordBO;
import com.vmo.notes.userhistorypassword.form.HistoryPasswordForm;

public interface IHistoryPasswordService extends ICommonService<HistoryPasswordBO, HistoryPasswordBean, HistoryPasswordForm> {
}
