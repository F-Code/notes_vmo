package com.vmo.notes.userhistorypassword.service.impl;

import com.vmo.notes.utils.CommonService;
import com.vmo.notes.utils.Constants;
import com.vmo.notes.utils.DataTableResults;
import com.vmo.notes.userhistorypassword.bean.HistoryPasswordBean;
import com.vmo.notes.userhistorypassword.bo.HistoryPasswordBO;
import com.vmo.notes.userhistorypassword.dao.HistoryPasswordDao;
import com.vmo.notes.userhistorypassword.form.HistoryPasswordForm;
import com.vmo.notes.userhistorypassword.service.IHistoryPasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class HistoryPasswordService implements IHistoryPasswordService {
    @Autowired
    private HistoryPasswordDao historyPasswordDao;

    @Autowired
    private CommonService commonService;

    @Autowired
    private BCryptPasswordEncoder bcryptEncoder;

    /**
     * Get password
     * @return
     */
    public List<HistoryPasswordBO> findAll() {
        List<HistoryPasswordBO> list = new ArrayList<>();
        historyPasswordDao.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    /**
     * findById
     *
     * @param id
     * @return
     */
    public HistoryPasswordBO findById(Long id) {
        return historyPasswordDao.findById(id).orElse(null);
    }

    /**
     * getDatatables
     *
     * @param historyPasswordForm
     * @return
     */
    public DataTableResults<HistoryPasswordBean> getDatatables(HistoryPasswordForm historyPasswordForm) {
        return historyPasswordDao.getDatatables(commonService, historyPasswordForm);
    }

    /**
     * saveOrUpdate
     *
     * @param entity
     */
    @Transactional
    public void saveOrUpdate(HistoryPasswordBO entity) {
        historyPasswordDao.save(entity);
    }


    /**
     * delete
     *
     * @param entity
     */
    public void delete(HistoryPasswordBO entity) {
        historyPasswordDao.delete(entity);
    }


    public HistoryPasswordBO save(HistoryPasswordForm history) {
        HistoryPasswordBO hisPassword = new HistoryPasswordBO();
        hisPassword.setUserId(history.getUserId());
        hisPassword.setPassword(bcryptEncoder.encode(history.getPassword()));
        hisPassword.setCreatedAt(new Date());
        hisPassword.setCreatedBy("admin");
        hisPassword.setIsDeleted(Constants.ACTIVE.IS_ACTIVE);
        return historyPasswordDao.save(hisPassword);
    }

     public HistoryPasswordBO findPassword(Long idUser){
        return historyPasswordDao.findPassword(idUser);
     }
}
