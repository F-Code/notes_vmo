package com.vmo.notes.userhistorypassword.controller;

import com.vmo.notes.userhistorypassword.bo.HistoryPasswordBO;
import com.vmo.notes.userhistorypassword.form.HistoryPasswordForm;
import com.vmo.notes.userhistorypassword.service.impl.HistoryPasswordService;
import com.vmo.notes.utils.CommonCRUD;
import com.vmo.notes.utils.Constants;
import com.vmo.notes.utils.ICommonService;
import com.vmo.notes.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.function.Supplier;

@CrossOrigin("*")
@Controller
@RequestMapping("/api/historyPassword")
public class HistoryPasswordController {
    @Autowired
    private HistoryPasswordService historyPasswordService;

    private Supplier<HistoryPasswordBO> supplier = new Supplier<HistoryPasswordBO>() {
        @Override
        public HistoryPasswordBO get() {
            return new HistoryPasswordBO();
        }
    };
    @Autowired
    private BCryptPasswordEncoder bcryptEncoder;

    /**
     * findById
     *
     * @param id
     * @return
     */
    @GetMapping(path = "/{id}")
    public @ResponseBody
    Response findById(@PathVariable Long id) {
        CommonCRUD<HistoryPasswordBO> commonCRUD = new CommonCRUD((ICommonService) historyPasswordService, supplier);
        HistoryPasswordBO historyPasswordBO = historyPasswordService.findById(id);
        if (commonCRUD.checkNullAndNotActive(historyPasswordBO) == false) {
            return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
        }

        return Response.success("Get Data Successfully").withData(historyPasswordBO);
    }

    /**
     * processSearch
     *
     * @param form
     * @return DataTableResults
     */
    @PostMapping(path = "/get-all")
    @PreAuthorize("hasAuthority('USER')")
    public @ResponseBody
    Response processSearch(@RequestBody HistoryPasswordForm form) {
        return Response.success("Get Data List Successfully")
                .withData(historyPasswordService.getDatatables(form));
    }

    @PostMapping(path = "")
    @ResponseBody
    public Response saveHis(@RequestBody HistoryPasswordForm form) {
        // Validate email
        HistoryPasswordBO historyPasswordBO = historyPasswordService.save(form);
        historyPasswordBO.setUserId(form.getUserId());
        historyPasswordBO.setPassword(bcryptEncoder.encode(form.getPassword()));
        return Response.success("thành công").withData(historyPasswordBO);
    }

    /**
     * delete
     *
     * @param id
     * @return
     */
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    Response delete(@PathVariable Long id) {
        CommonCRUD<HistoryPasswordBO> commonCRUD = new CommonCRUD((ICommonService) historyPasswordService, supplier);
        return commonCRUD.delete(id);
    }
}
