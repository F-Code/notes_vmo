package com.vmo.notes.userhistorypassword.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HistoryPasswordForm {

    private Long id;

    private Long userId;

    private String password;

    private Integer page;

    private Integer recordPage;

}
