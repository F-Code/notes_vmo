package com.vmo.notes.userhistorypassword.bean;

import com.vmo.notes.utils.CommonBean;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HistoryPasswordBean extends CommonBean {

    private Long id;

    private Long userId;

    private String password;

}
